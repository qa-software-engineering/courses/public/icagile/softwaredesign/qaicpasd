# Technical Leadership - Concepts

## Objectives

- Define technical leadership and what it entails. This should address the Why (do we need it), What (is it), How (do we do it), When (is it needed) and Who (should exhibit it).
- Introduce the fact that how the technical leader works with others affects the value and acceptance of the work they produce. This also includes looking at how some behaviours such as "ivory-tower-architecture" can be a hindrance rather than a help.
  
## Outcomes

By the end of this module, a learner can:

- Articulate the need for technical leadership and distinguish positive and negative characteristics of it.
- Explain the Why, What, How, When and Who of technical leadership.
- Demonstrate productive and un-productive technical leadership behaviours.

## Leadership - Part 1

<img src="../images/MLK.png" alt="Martin Luther King Jr" width=100 />

### Martin Luther King Jr

> “A genuine leader is not a searcher for consensus but a moulder of consensus.”

<img src="../images/RC.png" alt="Rosalynn Carter" width=100 />

### Rosalynn Carter

>“A leader takes people where they want to go. A great leader takes people where they don't necessarily want to go, but ought to be.”

---

## Responsibilities of a **Technical Leader** - Part 2

- Defining reality
- Maintaining the project technical vision clear for everyone, from beginning to end
- Keeping in mind most engineering details of a project or system
- Answering the technical questions that team or stakeholders have
- Helping the team resolve difficult engineering problems
- Reviewing the decisions the team makes to ensure consistency and alignment with the vision
- Translating engineering realities into simple terms for non-technical stakeholders
- Allow the team to make progress by staying out of their way and protecting them from distractions
- Knowing team members strengths and weaknesses
- Mentoring team members
- Saying, “Thank you” to the team

[https://www.coderhood.com/11-top-responsibilities-and-10-common-mistakes-of-a-technical-leader/]

![Technical Leaders](../images/TL.png)

---

## The role of the Agile Leader - Part 3

- Remove noise and impediments
- Facilitate the Agile practices
- Be the Process Guardian or Agent of Change

In an Agile environment, the role of Agile Leader refers to the person that is responsible for the successful implementation of the Agile Manifesto. The Agile Leader is not a line-manager, but focuses on enabling Agility in the team.

Agile Leaders are responsible for:

- **Removing noise and impediments.** 'Noise' is anything that gets in the way of the team doing its work. The Team Leader is not responsible for single-handedly removing the root cause of noise, but to ensure that noise is being actively identified, reported and eliminated by the team or escalated accordingly.

- **Facilitation of the Agile environment.** In particular, the Team Leader will be responsible for facilitating the Agile practices and events such as the Daily Synchronisation Meeting or the Show and Tell.

- **Process Guardian (also referred to as Agent of Change).** In Agile environments based on the empirical process, it is expected that the Agile teams will inspect and adapt their ways of working. The Agile Leader plays a fundamental role in ensuring that this is happening in an orderly and effective manner.

In teams new to Agile, the role of the Team Leader is an intensive full-time activity. As teams mature, this role can often be absorbed by the Team members themselves.

## Business Culture and Agile

Ensuring the organisation is culturally ready to accept change is important

The *Schneider Culture Model* defines culture as how we answer the question, "how do we do things here to succeed?" It describes four distinct cultures:

- Collaboration culture is about working together
- Control culture is about getting and keeping control
- Competence culture is about being the best
- Cultivation culture is about learning and growing with a sense of purpose

Organisations will often exhibit a dominant culture and will exploit elements of the other cultures as a means to reinforce the dominant one.

Understanding the dominant culture of an organisation is a fundamental exercise before we embark on any significant change programme.

![Agile Culture](../images/AC.png)

**Further Reading:** ['The Reengineering Alternative,' William Schneider](https://books.google.co.uk/books/about/The_Reengineering_Alternative.html?id=8P4JAQAAMAAJ&redir_esc=y)

## Agile-friendly Business Culture

Mapping the Agile Manifesto values and principles into the Schneider Model primarily maps the Agile Manifesto as being closely aligned to the Collaboration and Cultivation cultures

There are some principles that fit into Competence

There are very few values or principles that fit into Control

![Agile-friendly Business Culture](../images/AFBC.png)

**Further Reading:** ['An Agile Adoption and Transformation Survival Guide,' Michael Sahota](https://www.google.co.uk/books/edition/An_Agile_Adoption_and_Transformation_Sur/Bz_fCgAAQBAJ?hl=en&gbpv=0)

## The Journey to Agility

The dominant culture of most traditional organisations is the Control culture. These are what we often refer to as 'command and control' organisations

As the core aspects of Agile are aligned to other cultures, attempting to complete an adoption of any Agile method poses a significant challenge to the organisation

When 'adopting Agile', what we are effectively forcing upon the organisation is to complete a cultural transformation towards an Agile-friendly culture

This is what we typically refer to as 'The Journey to Agility'

![The Journey to Agility](../images/JTA.png)

**Further Reading:** ['Agile Adoptions that Work and Last,' Jose Casal](https://www.slideshare.net/jcasal1/agile-adoptions-that-work-and-last-jose-casal-bcs-agile-methods-sg)

---

## Behaviours of a Good Technical Leader - Part 4

- Owns a project end-to-end
- Pushes improvements into lower layers
- Builds tools and has other engineers use them
- Acts as the point person
- Owns a service
- Keeps stakeholders informed
- Comes up with new techniques
- Provides meaningful code reviews
- Forms an opinion, and shares their point(s) of view
- Understands early or nascent work
- Becomes a person of recognized authority
- Is the first person on the ground
- Gets involved in architecture review sessions
- Gets the team excited about something
- Do something 10x better than anyone else
- Exhibits strong project management
- Helps their team raise the bar
- Takes ownership of something unmaintained
- Presents their work/team/product
- The person that resolves conflict
- Infects others with their obsession
- Helps their team bridge the technical gaps
- Inserts themselves into relevant conversations
- Excellent communication skills

[https://www.linkedin.com/pulse/25-ways-demonstrate-technical-leadership-lee-mallabone]

## Leadership Styles

![Leadership Styles](../images/LS.png)

>Sustain – Dealing with operational excellence: Technical Manager
>Transform – Doing more with less: Chief Engineer
>Modernize – Business Driven: Application or business architect
>Innovate – Visionary: Enterprise evangelist or ideally an Enterprise Architect

Charlie Bess from [https://diginomica.com/technical-leadership-styles-and-why-they-must-change-over-time]

## Motivated and Talented Individuals

![Motivated and Talented Individuals](../images/MTI.png)

**Autonomy:** Provide employees with autonomy over some (or all) of the four main aspects of work:

- When they do it (time) – focus on the output (results) rather than on a schedule
- How they do it (technique) – empower the team to decide how to complete their work
- Whom they do it with (team) – provide some choice in who the team works with
- What they do (task) – allow regular 'creative' days

**Mastery:** Allow employees to become better at something that matters to them:

- Provide *'Goldilocks tasks'* – tasks that allow us to develop our skills further
- Create an environment where mastery is possible – foster a learning environment

**Purpose:** Take steps to fulfil employees' natural desire to contribute to a cause greater and more enduring than themselves:

- Communicate the purpose – provide a vision for the organisation
- Emphasise purpose maximisation – organisational and individual goals should focus on purpose as well as profit
- Use purpose-oriented words – talk about the organisation as a united team; 'us' and 'we'

**Futher reading:** ['Drive: The Surprising Truth About What Motivates Us,' Dan Pink, 2012](https://www.amazon.co.uk/Drive-Surprising-Truth-About-Motivates-ebook/dp/B0033TI4BW#:~:text=In%20DRIVE%2C%20he%20reveals%20the,of%20something%20larger%20than%20ourselves.)

## Tuckman's Group Development

![Tuckman's Group Development](../images/TGD.png)

Bruce Tuckman's (1965) group development model details the stages a team goes through:

- **Forming** – the group is formed
- **Storming** – the group members jostle for position in the group
- **Norming** – the group settles into a way of working
- **Performing** – some groups reach the high-performing state
- **Adjourning** – the group is disbanded

To allow a team to reach its Performing stage capabilities, it is fundamental that the team works together and is stable for as long as possible

When anything changes in the team's composition or nature, the team will spend time revisiting the previous stages

It is natural to see teams that are exhibiting elements of all stages, with one of them being the most dominant stage. Many teams encounter fairly regular changes in their environments and, as a result, they may never be able to advance past the Storming and Norming stages.

---

## Technical Leadership Mistakes - Part 5

- Forcing decisions on the team
- Not listening to the team
- Falling behind
- Losing the trust of the team
- Waiting to have all the data before making any decision
- Not being available to the team
- Being Misaligned With Executive Management
- Letting Pride, Ego, and Narcissism Guide Decisions
- Not Knowing Your People
- Giving a bad example

[https://www.coderhood.com/11-top-responsibilities-and-10-common-mistakes-of-a-technical-leader/]
