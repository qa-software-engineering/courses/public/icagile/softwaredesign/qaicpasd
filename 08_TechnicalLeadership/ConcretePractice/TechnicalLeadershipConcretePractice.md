# Technical Leadership - Concrete Practice

## Activity

### Case Study - Leadership Characteristics: Positive and Negative

In pairs, read through the transcript of the famous videos "I want to run an Agile Project - Parts 1 & 2" (if you really want to you can watch them instead but for everyone's sake please use a headset!)

[I want to run an Agile project transcript](IWantToRunAnAgileProjectTranscript.pdf)

As you read, identify and note positive and/or negative leadership behaviours in the scenario.

For negative leadership behaviours, suggest how each could have been handled differently.

Be prepared to share you findings.

*Whole Activity Time Box:* **20 minutes**
