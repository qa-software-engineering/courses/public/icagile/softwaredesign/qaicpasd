# Technical Leadership - Connections

## Context

>Technical leadership is more than just delivering a design.
>
>How we work as technical leadership is just as important as what we do.

## Questions - Part 1

How would you define “Leadership”?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 2

What are the responsibilities of a technical leader?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 3

What effect does Agile have on the role of the leader?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 4

What are the behaviours of a good technical leader?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 5

What mistakes might a technical leader make?
*Whole Activity Time Box:* **3 minutes**
