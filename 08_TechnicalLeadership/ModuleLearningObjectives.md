# Technical Leadership

## Objectives

- Define technical leadership and what it entails. This should address the Why (do we need it), What (is it), How (do we do it), When (is it needed) and Who (should exhibit it).
- Introduce the fact that how the technical leader works with others affects the value and acceptance of the work they produce. This also includes looking at how some behaviours such as "ivory-tower-architecture" can be a hindrance rather than a help.
  
## Outcomes

By the end of this module, a learner can:

- Articulate the need for technical leadership and distinguish positive and negative characteristics of it.
- Explain the Why, What, How, When and Who of technical leadership.
- Demonstrate productive and un-productive technical leadership behaviours.
