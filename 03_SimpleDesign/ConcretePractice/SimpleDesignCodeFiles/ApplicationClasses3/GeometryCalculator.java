public class GeometryCalculator {
	public double Area(Shape[] shapes) {
		double area = 0;
		for (Shape s : shapes) {
			area += s.Area();
		}
		return area;
	}
}
