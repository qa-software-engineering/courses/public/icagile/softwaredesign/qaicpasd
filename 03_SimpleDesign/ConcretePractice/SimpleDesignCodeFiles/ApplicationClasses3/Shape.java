// This could also be an interface - but Shape could have some properties added later
public abstract class Shape {
	public abstract double Area();
}
