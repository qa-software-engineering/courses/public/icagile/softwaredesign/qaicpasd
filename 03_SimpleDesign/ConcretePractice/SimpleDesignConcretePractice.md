# Simple Design - Concrete Practice

## Task 1 - Identifying SOLID

Look at the classes provided in the **ApplicationClassesX** folders (where **X** is a number from **1** to **4**).  

See if you can identify any **SOLID** principles that have been *broken* or *adhered to* within the application.

View the `.java` files with a text editor and add comments using `//` as a prefix.
e.g.

`// The Single Responsibility principle has been broken.`

*Whole Activity Time Box:* **20 minutes**
