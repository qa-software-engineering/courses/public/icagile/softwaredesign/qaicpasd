# Simple Design Concepts

## Objectives

- Illustrate the need for and value of "simple designs" in order to avoid over-engineering; when faced with alternatives, to choose the simpler of them. This should include several taste-tests for simple design, such as single responsibility, McCabe complexity, Beck's 4 rules of simple design, "whichever is easier-to-test", etc.
- Practice evaluating designs and applying concepts/techniques that make them easier to evolve. This should include practice in some of the known published principles and techniques: DRY principle, SOLID, etc.

## Outcomes

By the end of this module, a learner can:

- Apply principles and techniques for creating and evaluating simple designs
- Apply one or more techniques for simple design
- Apply one or more principles or techniques for evaluating designs

## Simple Design Quotes

<img src="../images/ASE.png" alt="Antoine de Saint-Expupéry" width=100 />

### Antoine de Saint-Exupéry

> “Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away.”

<img src="../images/AE.png" alt="Albert Einstein" width=100 />

### Albert Einstein

>"Any intelligent fool can make things bigger, more complex and more violent. It takes a touch of genius and a lot of courage to move in the opposite direction.”

<img src="../images/KB.png" alt="Kent Beck" width=100 />

### Kent Beck

>"**Appropriate for the intended audience.** It doesn't matter how brilliant and elegant a piece of design is; if the people who need to work with it don't understand it, it isn't simple for them.
>**Communicative.** Every idea that needs to be communicated is represented in the system. Like words in a vocabulary, the elements of the system communicate to future readers.
>**Factored.** Duplication of logic or structure makes code hard to understand and modify.
>**Minimal.** Within the above three constraints, the system should have the fewest elements possible. Fewer elements means less to test, document, and communicate."

---

### Simple Design

- **Y**ou **A**ren't **G**onna **N**eed **I**t (YAGNI)
- **D**on’t **R**epeat **Y**ourself (DRY)
- Once and Only Once

## SOLID

| Letter | Principle                       | Description                                                                      |
|--------|---------------------------------|----------------------------------------------------------------------------------|
| **S**  | Single Responsibility Principle | A class has one single responsibility and therefore one reason to change         |
| **O**  | Open/Closed Principle           | Open for extensions, closed for modifications                                    |
| **L**  | Liskov Substitution Principle   | Subtypes must be substitutable for their base type                               |
| **I**  | Interface Segregation Principle | Not one big interface for all clients, but one interface per kind of client      |
|        |                                 | No methods in interface that client does not use. No “fat” interfaces            |
| **D**  | Dependency Inversion Principle  | High-level modules should not depend on low-level modules.                       |
|        |                                 | Both should depend on abstractions.                                              |
|        |                                 | Abstractions should not depend on details. Details should depend on abstractions |

These principles are for better Object Oriented (OO) design. All of these principles lead to smaller classes, with clear responsibilities, that are easy to extend and hard to break. They lead to more easily-testable code. All of these things are important for scrum, since they help us write quality code. Combine that with a proper level of documentation in the code and proper naming and you've reached easily maintainable code.

Teams that don’t understand these principles (or at least don't follow them very well) can succeed as well. Some teams may try to use these principles and their flow grinds to a halt, so they're not the holy grail. Especially when you have a lot of less senior developer or developers with a non-OO background.

For other non-OO programming paradigms, similar principles exist, but they are beyond the scope of this course. So when you do VB6, F#, PHP or something else, you can still write highly maintainable code, but will probably not use some of these principles. Going to deep into this topic is rat hole.

The **SOLID** principles are five dependency management principles used in OO programming and design. The acronym was introduced from the work of Robert Martin. Each letter represents another three-letter acronym that describes one principle.
When working with software in which dependency management is handled badly, code can become rigid, fragile and difficult to reuse. Code which is difficult to modify, either to change existing functionality or add new features, susceptible to the introduction of bugs, particularly those that appear in a when another area of code is changed. If you follow the **SOLID** principles, you produce code that is more flexible and robust, with a higher possibility for reuse.

---

## Single Responsibility Principle

### Many Responsibilities (Undesirable)

```c#
public class BankAccount
{
  public string AccountNumber { get; set; }
  public decimal AccountBalance { get; set; }
 
  public decimal CalculateInterest()
  {
    return 42;
  }
 
  public void CloseAccount()
  {
    new AccountManager().Close(AccountNumber);
  }
}
```

### Single Responsibility (Desired)

```c#
public class BankAccount
{
  public string AccountNumber { get; set; }
  public decimal AccountBalance { get; set; }
}
 
public class InterestCalculator
{
  public decimal CalculateInterest(BankAccount acct)
  { return 42; }
}
 
public class AccountManager
{
  public void Close(int accountNumber)
  {  ... }
}
```

The ***Single Responsibility Principle (SRP)*** states that every class (or method, or module) should have a single responsibility, and that responsibility should be entirely encapsulated by the class (or method, or module). All its services should be narrowly aligned with that responsibility. A *“responsibility”* is a reason to change and a class or module should have one, and only one, such reason to change.
 
In the example above, the `BankAccount` class contains bank account data along with a method to calculate interest, and another method to close the bank account. The data properties of the class will need to change when the logical data model changes. The interest calculation method will need to change as new ways of accruing interest emerges. The `CloseAccount` method will need to change when the business process of terminating an account changes. All of these reasons to change are very different, and this class design will lead to very frequent changes to the class. With a suite of unit tests supporting the class, every change to the class might cause several unit tests to break, and with no clear separation of concerns, it will be more difficult to figure out whether a new bug was introduced, or if the unit tests need to change as well.

See [http://bit.ly/VV9WW7] for more information.

---

## Open/Closed Principle

### Not open for extensions (Undesirable)

```c#
public double Area(object[] shapes)
{
  double area = 0;
  foreach (var shape in shapes)
  {
    if (shape is Circle)
    {
      Circle circle = (Circle)shape;
      area += circle.Radius * 
        circle.Radius * Math.PI;
    }
    else if (shape is Rectangle)
    {
      // ...
    }
  }
  return area;
}
```

### Open for extensions (Desired)

```c#
public abstract class Shape
{
  public abstract double Area();
}
 
public class Circle : Shape
{
  public double Radius { get; set; }
  public override double Area()
  { return Radius * Radius * Math.PI; }
}
 
public double Area(Shape[] shapes)
{
  double area = 0;
  foreach (var shape in shapes)
  { area += shape.Area(); }
  return area;
}
```

The **Open/Closed principle** states that software classes and methods should be *open for extension*, but *closed for modification*. This means that an entity can allow its behaviour to be modified without altering its source code. This is especially valuable in a production environment, where changes to source code are very expensive because of testing, code reviews, packaging, and deploying. Code obeying this principle doesn't change when it is extended, and therefore needs no such effort. A common way of achieving compliance with the **Open/Closed Principle** is to separate parts of the code that changes often, from the parts of the code that rarely changes.

In the example above, the `Area` method calculates the summed area for an array of shapes. The shapes do not have a common base class except for `Object`, and the algorithm relies on detecting the type of each shape, and then use type-specific properties and methods to calculate the area. If you wanted to add support for a new type of shape, you would have to create a class for that shape, and then modify the `Area` method to detect the new shape. The need for adding support for new shapes is expected to happen more frequently, than the need for modifying the overall algorithm for calculating the summed area, or for calculating the area of a particular shape. Therefore, these parts of the code needs to be separated. By separating these parts of the code, the algorithm will hardly ever need to change (it is closed for modification), but it is easy to add support for new shapes (so it is open for extension).

See [http://bit.ly/doRVQt] for more information

---

## Liskov Substitution Principle

### Squares are rectangles...

```c#
public class Rectangle {
  public double Height { get; set; }
  public double Width { get; set; }
  public double CalculateArea() {
    return Height * Width;
  }
}
 
public class Square : Rectangle {
  double _EdgeLength;
  public double Height {
    get { return _EdgeLength; }
    set { _EdgeLength = value; }
  }
  public double Width {
    get { return _EdgeLength; }
    set { _EdgeLength = value; }
  }
}
```

### ...with only one variable (Desired)

```c#
public interface IShape {
  double CalculateArea();
}
 
public class Rectangle : IShape {
  public double Height { get; set; }
  public double Width { get; set; }
  public double CalculateArea() {
    return Height * Width;
  }
}
 
public class Square : IShape {
  public double EdgeLength { get; set; }
  public double CalculateArea() {
    return EdgeLength * EdgeLength;
  }
}
```

The **Liskov Substitution principle** states that if `Class2` is a *subtype* of `Class1`, then objects of type `Class1` may be replaced with objects of type `Class2`. In other words, objects of type `Class2` may be substituted for objects of type `Class2` without altering any of the desirable properties, such as correctness, tasks performed, etc. The principle says that you should always be able to use a subclass instead of the base class and still get the expected result. If you can't, you’re breaking the principle.
 
In the example above, the `Square` class is a subclass of the `Rectangle` class, as a square is a special kind of rectangle. However, a square does not need two different side lengths like a rectangle does, and the `Square` class has therefore changed the implementation of the `Width` and `Height` properties to both manipulate the same member variable. On the surface everything looks fine, but code might exist, that works from the assumption that modifying the `Height` of a `Rectangle` does not cause changes to the `Width` and vice versa. Code like that will break when a `Square` is used instead of a `Rectangle`, because the `Square` class does not behave like its base class `Rectangle`.

See [http://bit.ly/UWYpG7\ for more information.

---

## Interface Segregation Principle

### Not all workers need to eat

```c#
interface IWorker {
  public void Work();
  public void Eat();
}
 
class Worker : IWorker {
  public void Work() { ... }
  public void Eat() { ... }
}
 
class SuperWorker : IWorker {
  public void Work() { ... }
  public void Eat() { ... }
}
 
class RoboWorker : IWorker {
  public void Work() { ... }
  public void Eat() { throw new RoboException(); }
}
```

### Work/eat segregation

```c#
interface IWorker {
  public void Work();
}
 
interface IEater {
  public void Eat();
}
 
class Worker : IWorker, IEater {
  //...
}
 
class SuperWorker : IWorker, IEater {
  //...
}
 
class RoboWorker : IWorker {
  //...
}
```

The **Interface-Segregation Principle (ISP)** demands that no client should be forced to depend on methods it does not use. This principle is used for clean development and intends to make software easy-to-change. **ISP** keeps a system decoupled and thus easier to refactor, change, and redeploy. **ISP** splits interfaces which are very large into smaller and more specific ones so that clients will only have to know about the methods that are of interest.

In the example above, the `IWorker` interface defines two methods, `Work` and `Eat`, and the three classes `Worker`, `SuperWorker` and `RoboWorker` all implement this interface. However, a `RoboWorker` does not need to eat, so the implementation of the `Eat` method just throws an exception. This is an obvious symptom of the interface having too many methods, since any recipient of an `IWorker` object will expect the object to be able to both eat and work, which is not the case.

For more information visit [http://bit.ly/lbeMAL]

---

## Dependency Inversion Principle

### Class-to-class dependency

```c#
Using MyApp.DataAccessLayer;
namespace MyApp.ServiceLayer
{
  public class CoolService
  {
    public CoolThing GetCoolThing(int id)
    {
      CoolSQLRepository repository = 
        new CoolSQLRepository();
      return repository.GetThing(id);
    }
  }
}
```

### Dependency Inverted

```c#
namespace MyApp.ServiceLayer
{
  public class CoolService
  {
    private IRepository _MyRepository;
    public CoolService(IRepository repository)
    {
      _MyRepository = repository;
    }
    public CoolThing GetCoolThing(int id)
    {
      return _MyRepository.GetThing(id);
    }
  }
  public interface IRepository
  {
    CoolThing GetThing(int id);
  }
}
```

### Dependency Inversion Principle

Most modern applications have several layers, services, and components. Each of these can be dependent on another, such as the UI depending upon a business logic layer, or that layer depending upon a data access layer. These dependencies, while quite common, result in tightly-coupled code which is difficult to maintain and to isolate while testing. Dependency inversion is more of a design approach than a pattern. Every professional software developer that values maintainable code should know and follow this principle.

### Dependency Injection

**Dependency Injection (DI)** is a way of doing **Dependency Inversion**. **DI** is a design pattern that allows the choice of component to be made at run-time rather than compile time. This is helpful for dynamically loading plugins or mock vs. real objects. Microsoft Unity is a lightweight, extensible dependency injection container for .NET applications. It supports interception, constructor injection, property injection, and method call injection. You can use Unity in a variety of different ways to help decouple the components of your applications, to maximize coherence in components, and to simplify design, implementation, testing, and administration of these applications. For more information on Unity visit [http://bit.ly/b1lRjM].

In the example above, the `CoolService` has a method `GetCoolThing`, that retrieves an object from a database. When trying to unit test that method, a database needs to be present, and the contents of that database need to be well known by the unit test. A unit test calling out to a database is in general a suboptimal choice, both because is incurs a execution duration penalty, but also because it makes it harder to establish a test harness for the code. The root cause of the problem in the code example is that the `GetCoolThing` method itself instantiates the `CoolSQLRepository` object. If this object instead had been added as a method parameter, it would be the responsibility of the caller of the method to make this instantiation. The dependency on the `CoolSQLRepository` will then have been moved from `GetCoolThing` method to the caller of that method. If this methodology is followed through the entire code base, all dependencies will eventually be passed into the objects or methods that need them, and the responsibility for creating these object has been transferred to the outermost levels of the architecture. The result is, that when the application is running in production, production objects can be instantiated, whereas when unit tests are run, mocks and fakes can be created instead.
