# Simple Design

## Overview

This learning module is to illustrate the need for and value of "simple designs" in order to avoid over-engineering; when faced with alternatives, to choose the simpler of them. This should include several taste-tests for simple design, such as single responsibility, McCabe complexity, Beck's 4 rules of simple design, "whichever is easier-to-test", etc.  It also encourages practising evaluating designs and applying concepts/techniques that make them easier to evolve. This should include practice in some of the known published principles and techniques: DRY principle, SOLID, etc.
These can be used as part of teaching and/or as the base for video content to be housed on Cloud Academy.  MD can be converted to PPTX using Pandoc.

## Concrete Practice

This provides a guided activity to allow learners to demonstrate their skills and understanding of the topic area.

## Code Files

Use the command from the root of the project:
<!-- 
```zsh

git subtree pull --prefix ConcretePractice/BuildToolsCodeFiles https://gitlab.com/qa-software-engineering/learning-modules/ci-cd-cd/build-tools-code-files master --squash

``` -->

This will update any changes to the Code Files in this repo.
