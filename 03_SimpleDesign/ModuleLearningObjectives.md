# Simple Design

## Objectives

- Illustrate the need for and value of "simple designs" in order to avoid over-engineering; when faced with alternatives, to choose the simpler of them. This should include several taste-tests for simple design, such as single responsibility, McCabe complexity, Beck's 4 rules of simple design, "whichever is easier-to-test", etc.
- Practice evaluating designs and applying concepts/techniques that make them easier to evolve. This should include practice in some of the known published principles and techniques: DRY principle, SOLID, etc.

## Outcomes

By the end of this module, a learner can:

- Apply principles and techniques for creating and evaluating simple designs
- Apply one or more techniques for simple design
- Apply one or more principles or techniques for evaluating designs
