# Simple Design - Connections

## Context

>It is natural to think that the more complicated design is the better one; however, simpler designs better retain agility in the codebase and therefore also in the company.
>
>If it is still not known how to invent good designs, quite a lot is known about how to evaluate designs.

## Questions

What design principles and techniques can you think of?
*Whole Activity Time Box:* **3 minutes**

Under what circumstances might a principal such as DRY or SOLID conflict with simple design?
*Whole Activity Time Box:* **3 minutes**

## Activities

Apply "YAGNI" to technical work backlog. Use this backlog (from the Scrum Alliance website back in 2003 from [https://www.mountaingoatsoftware.com/agile/scrum/scrum-tools/product-backlog/example]:

### Profiles

| User Story                                                                                                                                                                                                                                                                     |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| As a site member, I want to describe myself on my own page in a semi-structured way so that others can learn about me. That is, I can fill in predefined fields, but also have room for a free-text field or two. (It would be nice to let this free text be HTML or similar.) |
| As a site member, I can fill out an application to become a Certified Scrum Practitioner so that I can earn that designation. [Note: Certified Scrum Practitioner was the initial name of what became known as Certified Scrum Professional.]                                  |
| As a Practitioner, I want my profile page to include additional details about me (i.e., some of the answers to my Practitioner application) so that I can showcase my experience.                                                                                              |
| As a site member, I can fill out an application to become a Certified Scrum Trainer so that I can teach CSM and CSPO courses and certify others.                                                                                                                               |
| As a Trainer, I want my profile page to include additional details about me (i.e., some of the answers to my Trainer application) so that others can learn about me and decide if I’m the right trainer for them.                                                              |
| As a Practitioner or Trainer, when I write an article for the site I want a small graphic shown with the article showing my CSP or CST status so that others know my certifications when reading. (For example, Amazon’s “Top 500 Reviewers” approach.)                        |
| As a trainer, I want my profile to list my upcoming classes and include a link to a detailed page about each so that prospective attendees can find my courses.                                                                                                                |
| As a site member, I can view the profiles of other members so that I can find others I might want to connect with.                                                                                                                                                             |
| As a site member, I can search for profiles based on a few fields (class attended, location, name) so I can find others I might want to connect with.                                                                                                                          |
| As a site member, I can mark my profile as private in which case only my name will appear so that no one can learn things about me I don’t want shared.                                                                                                                        |
| As a site member, I can mark my email address as private even if the rest of my profile is not so that no one can contact me.                                                                                                                                                  |
| As a site member, I can send an email to any member via a form so that we can connect.                                                                                                                                                                                         |
| As a site administrator, I can read practising and training applications and approve or reject them so that only applicants who qualify can become CSPs or CSTs.                                                                                                               |
| As a site administrator, I can edit any site member profile so that I can correct problems for members.                                                                                                                                                                        |

*Whole Activity Time Box:* **10 minutes**
