# Design for Automated Testing

## Objectives

- Practice and apply strategies and techniques for automating acceptance tests without using the user interface, including the architectural support required.
- Explain strategies and techniques for building maintainable and intention-revealing automated acceptance tests when it is not possible or practical to bypass the user interface. This includes using adapters or wrappers around the user interface so that good UI-agnostic acceptance tests can be used.
- Describe the role of test automation in verifying cross-functional attributes, including capacity and response time, security, etc., and how to fit these into an Agile process.

## Outcomes

By the end of this module, a learner can:

- Build and apply automated tests that exercise the UI, bypass the UI and assess non-functional aspects of a product.
- Apply and/or define an architecture supporting automated testing that bypasses the UI, and writing acceptance tests that exploit that support.
- Explain how to design effective UI-driven acceptance tests.
- Categorize and explain how non-functional testing is included in test automation design.
