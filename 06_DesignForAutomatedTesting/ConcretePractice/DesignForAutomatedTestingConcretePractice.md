# Design for Automated Tests - Concrete Practice

## Automated UI Testing - using Selenium

**Objective:**

- To be able to use Selenium and a JavaScript testing framework to drive some automated UI testing

**Overview:**

This exercise uses **Selenium**'s *webdriver* for **Chrome** and the *JavaScript* testing framework **Jest**. The skeleton code for setting up the test environment has already been done.  You will need to make the tests:

- open a browser instance
- navigate to a website
- locate specific elements on the page
- perform some UI actions
- assert that the results of the actions are the expected outcome

### Task 1 - Use Selenium to drive a browser automatically and Jest to perform a test

1. Open up the folder `starter` from `06_DesignForAutomatedTesting/ConcretePractice/DesignForAutomatedTestingCodeFiles/selenium` in an IDE such as **VSCode**
2. Open the folder root in a terminal and execute the command `npm i`
3. Wait for this process to finish
4. Open up `search.test.js` in the IDE for editing
5. Locate the comment `// Get selenium to open Chrome and go to DuckDuckGo` in the `beforeEach` part of the test
6. Under this comment add a line that assigns `driver` to be the result of `await new Builder().forBrowser("chrome").build()`
  
```javascript
  let driver = await new Builder().forBrowser("chrome").build();
  
```

7. In the **Act** part of the test:
   1. Under the comment `// Get Selenium to go to DuckDuckGo` add a line to `await` the `driver`'s call to `get` that receives `"http://duckduckgo.com"` as an argument
   2. , use `await` and `driver.findElement()`, passing in `By.name("q")` and chain a call to `sendKeys()` - passing in `searchString` and `Key.RETURN`

```javascript
  await driver.get("http://duckduckgo.com");
  await driver.findElement(By.name("q")).sendKeys(searchString, Key.RETURN;
```

8. In the **Assert section**, use a call to `expect()` passing in `await driver.getTitle()` and chain a call `toContain()` passing in `searchString`

```javascript
  expect(await driver.getTitle()).toContain(searchString);
```

9. Save the file.
10. Open the terminal and type `npm test`

This should start the test engine, open a Chrome browser, do the specified navigation and return a passing test!

**Note:** If there is a slow internet connection the test might time out - press `a` in the terminal to initiate another test run.

**Note:** To exit the test runner in the terminal, press `q` or `CTRL+C` on the keyboard to return to a prompt.

### Task 2 - Get Selenium to use a click on the Search button

1. Use the first test as a model to write another test that clicks the search icon to initiate the search rather than a `RETURN` key press.

**Hints:**

- Don't include the `Key.RETURN` argument
- Add another call that finds the element with the `id` of `search_button_homepage` - you could find this by inspecting the page and locating the search button in the **Dev Tools** in the browser
- Use the `click()` function on the button element to make Selenium perform a click on this element
- You can find the code in the `solution` folder to help if you get stuck

## Testing UIs without a Browser

- To be aware of how UIs can be tested without rendering into a browser

**Overview:**

This exercise uses `ReactJS` and `@testing-library/react`.  The skeleton code for setting up the test environment has already been done.  You will need to make the tests:

- Render the component (First test & Second Test)
- Check to see whether the correct content would appear on the screen (First test)
- Simulate a user "click" on a button on the page
- Check to see if the content changes as a result of the click

The Acceptance Criteria for this UI is that the user is able to change the text displayed in the link from `Learn React` to `Learning Browserless Testing` by clicking on the button provided on the page.

### Steps

1. Open up the folder `starter` from `06_DesignForAutomatedTesting/ConcretePractice/DesignForAutomatedTestingCodeFiles/testing-without-browser` in an IDE such as **VSCode**
2. Open the folder root in a terminal and execute the command `npm i`
3. Wait for this process to finish
4. Run the command `npm start` and examine the application in the browser
5. Click the inviting button `Click to change` and observe the output
6. Close the browser
7. In the terminal, run the command `npm test`
8. Observe that there is 1 passing and 1 skipped test - that is what you are going to write!
9. Open the file `/src/App.js`
   - This is the code that renders what you saw in the browser
   - Note what is supposed to happen when the `changeButton` is clicked - i.e it changes the state of linkText and this gets updated in the UI (by the magic of React that we're not going into!)
10. Open the file `/src/App/test.js`
    - This is the file the tests are run from
    - Note that the first test locates the element with the text `learn react` in it and asserts that it is in the rendered document
    - The second test is skipped by virtue of the `x` at the start of `test`
11. Remove the `x` so this test is no longer skipped
12. In the **Arrange** section, under the comment `// Render the App component` , use a call to `render` passing in the `App` component:

```jsx
  render(<App />)
```

13.  Under the comment `// Locate the changeButton element`, assign the `const` `changeButton` to be a call on `screen` with `getByTestId` using the argument `changeButton`

```jsx
  const changeButton = screen.getByTestId(`changeButton`);
```

14. In the **Act** section, under the comment `// Simulate a click on changeButton`, use `@testing-library/react`'s `fireEvent` calling `click` on it and passing in `changeButton`

```jsx
  fireEvent.click(changeButton);
```

15. In the **Assert** section, under the comment `// Get the link element by the text it now contains`, add code to set a `const` called `linkElement` to be a call of `getByText` on `screen`, passing in the regular expression `/learning browserless testing/i`

```jsx
  const linkElement = screen.getByText(/learning browserless testing/i);
```

- Remember that this is the change that clicking the button made in the browser, so we are testing this behaviour!

16.  Under the comment `// Expect the linkElement was found`, call `expect` passing in `linkElement` and chaining `toBeInTheDocument`

```jsx
  expect(linkElement).toBeInTheDocument();
```

17. Save the file
18. Check the new test passes (type `npm test` in the terminal if you need to)

Both of these activities could be run as part of the "Test" cycle in a DevOps pipeline as an automated process.

*Whole Activity Time Box:* **30 minutes**