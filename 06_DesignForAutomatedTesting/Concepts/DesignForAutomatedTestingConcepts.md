# Design for Automated Testing - Concepts

## Objectives

- Practice and apply strategies and techniques for automating acceptance tests without using the user interface, including the architectural support required.
- Explain strategies and techniques for building maintainable and intention-revealing automated acceptance tests when it is not possible or practical to bypass the user interface. This includes using adapters or wrappers around the user interface so that good UI-agnostic acceptance tests can be used.
- Describe the role of test automation in verifying cross-functional attributes, including capacity and response time, security, etc., and how to fit these into an Agile process.

## Outcomes

By the end of this module, a learner can:

- Build and apply automated tests that exercise the UI, bypass the UI and assess non-functional aspects of a product.
- Apply and/or define an architecture supporting automated testing that bypasses the UI, and writing acceptance tests that exploit that support.
- Explain how to design effective UI-driven acceptance tests.
- Categorize and explain how non-functional testing is included in test automation design.

## Automated Testing - Part 1

![Manual vs Automated Testing](../images/ManVAuto.png)

## Test Automation Tools

- Selenium
- Katalon Studio
- Unified Function Testing – UFT
- TestComplete
- Watir
- IBM Rational Functional Tester
- Ranorex

## Selenium

![Selenium Logo](../images/selenium.png)

- Open source
- Portable
- Cross browser support
- Cross operating support
- Suite of tools including Webdriver, and IDE (Firefox plug-in)
  - Can record user actions with Selenium IDE to generate script
  - Selenium Grid permits parallel testing on multiple machines and browsers
- Enables testing of web applications more effectively

![Selenium Testing](../images/selenium-test.png)

---

## Bypassing User Interfaces in Tests - Part 2

## Model, View, View-Model Pattern

![Model, View, View-Model Pattern](../images/MVVM.png)

[https://sureshkumarveluswamy.wordpress.com/2010/07/28/mvvm-design-pattern/]

## Headless Browser

- How Selenium operates

## Testing without a DOM

- JavaScript frameworks, like Jest and Jasmine can test without the need for browser rendering
  - Especially prevalent in React and Angular development
    - Snapshot tests generate a JavaScript-type object that can be compared against a saved snapshot
    - Support for TestBeds and rendering of components without the need for a browser
  
## End-to-End (e2e) Testing

- Frameworks like Cypress and libraries like **puppeteer** allow navigation across multiple routes and assertions on many side-effects, not only from the browser but potentially on the backend too.

## UI-driven Acceptance Testing

Planning of Acceptance Testing should include common components such as Scope, Approach, Test Environment, Resources, Responsibilities, Acceptance Test references, Entry and Exit criteria, Tools, etc.

Acceptance Test planning factors result in a business decision.  It needs to be agreed, reviewed and approved before running the tests.  The reviewing will usually be done by Product Owners in conjunction with the customer.

![Key Points for Acceptance Test Planning](../images/EffecttiveUATDesign.png)

As part of this plan, it can be decided what can be tested without browser rendering, what can be tested using a tool like Selenium and highlight any areas where difficulties will exist in test automation.  For the most part, all tests should be fully automated where possible.

![Sample Acceptance Tests](../images/SampleUIATs.png)

Images taken from [https://www.softwaretestinghelp.com/what-is-user-acceptance-testing-uat/#User_Acceptance_Testing_Design]

---

## Non-Functional Requirements (NFRs) - Part 3

- Performance
- Response time
- Security
- Concurrent requests
- Transactions per second
- Response times
- Sub-service failures
- Load
- Quality attributes
- etc

## NFR Testing Steps

- Be Agile - Don’t wait till the end
- Plan
- Setup
- Perform - Run your NFR tests
- Record (your results)

[https://capgemini.github.io/testing/how-to-do-nft/]

## How can these NFR tests be automated?

Test automation tools!!!

### Performance and Load Testing

According to [Software Testing Help](https://www.softwaretestinghelp.com/performance-testing-tools-load-testing-tools/), the 15 best performance and load testing tools in 2021 are:

- WebLOAD
- LoadNinja
- SmartMeter.io
- ReadyAPI Performance
- StormForge
- LoadView
- Apache JMeter
- LoadRunner
- Appvance
- NeoLoad
- LoadComplete
- WAPT
- Loadster
- k6
- Rational Performance Tester
- Testing Anywhere

### Security Testing Tools

The OWASP project lists sets of tools for assessing applications in:

- [Vulnerability - often referred to as DAST](https://owasp.org/www-community/Vulnerability_Scanning_Tools)
  - (Dynamic Application Security Testing)
- [ Source Code Analysis - often referred to as SAST](https://owasp.org/www-community/Source_Code_Analysis_Tools)
  - (Static Application Security Testing)

Many of these can be integrated into CI/CD pipelines
