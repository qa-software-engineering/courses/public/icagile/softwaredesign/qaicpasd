# Design for Automated Testing - Connections

## Context

>Too many people think that testing a system requires going through the user interface.
>
>Not being able to test bypassing the user interface does not mean that the ATDD strategies cannot be used.
>
>It is a common misconception and bad habit to think that testing non-functional attributes (also called cross-functional testing) gets done only at the end of the project.

## Questions - Part 1

What are the benefits of automating testing?
*Whole Activity Time Box:* **3 minutes**

What tools may be used to test the User Interface?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 2

What patterns enable tests that bypasses the User Interface and how?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 3

What non-functional requirements NFRs can you identify?
*Whole Activity Time Box:* **3 minutes**

What are the risks of delaying NFR testing until late in development?
*Whole Activity Time Box:* **3 minutes**