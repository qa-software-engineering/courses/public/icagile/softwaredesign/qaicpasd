# Patterns (Design and Architecture) - Connections

## Context

>Design patterns are idioms that solve common or difficult design problems.
>
>Architecture patterns address the broad structure or big picture of the system.

## Questions

**Quiz:** 10 Questions on common design patterns - which group do they belong to?
*Whole Activity Time Box:* **5 minutes**

**Question:** What different architecture patterns can you think of?
*Whole Activity Time Box:* **3 minutes**

**Quiz:** Which component of MVC contains…
*Whole Activity Time Box:* **3 minutes**
