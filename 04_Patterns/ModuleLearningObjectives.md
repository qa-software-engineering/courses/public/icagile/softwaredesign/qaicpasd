# Patterns (Design and Architecture)

## Objectives

- Teach and practice a few of the basic and well-known design patterns. This includes balancing use of design patterns with "simple design", noting that design patterns can complicate the design unnecessarily; and learning to refactor to design patterns
- Introduce a few of the basic and well-known architecture patterns. The patterns discussed should include layers, model-view-controller and microservices.

## Outcomes

By the end of this module, a learner can:

- Evaluate and select from among well-known design and architecture patterns based on project and technical context
- Discuss and evaluate well-known design patterns
- Compare and contrast well-known architecture patterns
