# Patterns (Design and Architecture)

## Overview

This learning module is to teach and practice a few of the basic and well-known design patterns. This includes balancing use of design patterns with "simple design", noting that design patterns can complicate the design unnecessarily; and learning to refactor to design patterns.  It also introduces a few of the basic and well-known architecture patterns. The patterns discussed should include layers, model-view-controller and microservices.
These can be used as part of teaching and/or as the base for video content to be housed on Cloud Academy.  MD can be converted to PPTX using Pandoc.

## Concrete Practice

This provides a guided activity to allow learners to demonstrate their skills and understanding of the topic area.

## Code Files

Use the command from the root of the project:
<!-- 
```zsh

git subtree pull --prefix ConcretePractice/BuildToolsCodeFiles https://gitlab.com/qa-software-engineering/learning-modules/ci-cd-cd/build-tools-code-files master --squash

``` -->

This will update any changes to the Code Files in this repo.
