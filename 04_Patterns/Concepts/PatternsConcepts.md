# Patterns (Design and Architecture) - Concepts

## Objectives

- Teach and practice a few of the basic and well-known design patterns. This includes balancing use of design patterns with "simple design", noting that design patterns can complicate the design unnecessarily; and learning to refactor to design patterns
- Introduce a few of the basic and well-known architecture patterns. The patterns discussed should include layers, model-view-controller and microservices.

## Outcomes

By the end of this module, a learner can:

- Evaluate and select from among well-known design and architecture patterns based on project and technical context
- Discuss and evaluate well-known design patterns
- Compare and contrast well-known architecture patterns

---

## What are Software Design Patterns

- Emerge over time as developers write code and establish common best practices for certain situations
- Usually solve a particular problems in generation and interactions of code rather than large scale architecture problems
- Powerful if used in the correct way applied in the correct circumstance
  - Not prescriptive specifications for writing code

---

## Software Design Patterns Embody Principles

- A great way of grasping and implementing principles in your code
- Have to strike balance:

![Design Pattern Balance](./images/Balance.png)

*How do you find that balance?*

---

## Common Design Patterns

These include:

- Facade
- Proxy
- Command
- Observer (subsumed into .NET as events)
- State
- Strategy/Template Method
  - Defines a family of algorithms, encapsulates each algorithm and makes the algorithms interchangeable within that family
- Factory
  - Creates objects without exposing the instantiation logic to the client
- Singleton
  - A class self-instantiates the only instance that exists in the program

These are some of the patterns included in the so-called "Gang of Four".

## Gang of Four

### Who were the "Gang of Four"

*Erich Gamma*, *Richard Helm*, *Ralph Johnson* and *John Vlissides* wrote a book called **Design Patterns: Elements of Reusable Object-Oriented Software** in 1994.

The publishers describe the book as follows:

>Capturing a wealth of experience about the design of object-oriented software, four top-notch designers present a catalog of simple and succinct solutions to commonly occurring design problems. Previously undocumented, these 23 patterns allow designers to create more flexible, elegant, and ultimately reusable designs without having to rediscover the design solutions themselves.
>
>The authors begin by describing what patterns are and how they can help you design object-oriented software. They then go on to systematically name, explain, evaluate, and catalog recurring designs in object-oriented systems. With Design Patterns as your guide, you will learn how these important patterns fit into the software development process, and how you can leverage them to solve your own design problems most efficiently.
>
>Each pattern describes the circumstances in which it is applicable, when it can be applied in view of other design constraints, and the consequences and trade-offs of using the pattern within a larger design. All patterns are compiled from real systems and are based on real-world examples. Each pattern also includes code that demonstrates how it may be implemented in object-oriented programming languages like C++ or Smalltalk.

---

## Gang of Four Patterns

There are 23 patterns described in the book.  These have been categorised into 3 sections:

**(TLDR;)** - [Gang of Four Design Patterns Reference Sheet](GangOfFour.pdf)
This document is freely downloadable from [http://www.blackwasp.co.uk/GangOfFour.aspx]

- Creational Patterns
  - Provide ways to create instances of individual or groups of related objects
  - **5** listed patterns:
    - Singleton
    - Factory
    - Abstract Factory
    - Builder
    - Prototype
- Structural Patterns
  - Lets developers establish relationships between classes or objects through the use of inheritance and interfaces
  - **7** listed patterns:
    - Adapter
    - Bridge
    - Composite
    - Decorator
    - Facade
    - Flyweight
    - Proxy
- Behavioural Patterns
  - Defines how classes and objects are allowed to communicate with each other
  - **11** listed patterns:
    - Chain of Responsibility
    - Command
    - Interpreter
    - Iterator
    - Mediator
    - Memento
    - Observer
    - State
    - Strategy
    - Template Method
    - Visitor

---

# Architecture Patterns

## Common Architectural Patterns

Layered
Client Server
Master Slave
Pipe-Filter
Broker
Peer to Peer
Event Bus
Model View Controller
Blackboard
Interpreter

[https://towardsdatascience.com/10-common-software-architectural-patterns-in-a-nutshell-a0b47a1e9013]
