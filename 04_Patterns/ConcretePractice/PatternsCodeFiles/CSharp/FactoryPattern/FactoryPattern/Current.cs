using System;
namespace FactoryPattern
{
	public class Current : Account {
	
	private int overdraftLimit;

		public int OverdraftLimit { get => overdraftLimit; private set => overdraftLimit = value; }

	public Current(int balance, String name, int overdraftLimit) {
		this.balance = balance;
		this.name = name;
		this.OverdraftLimit = overdraftLimit;
	}
	
	

	
	public override bool Withdraw(int amt) {
		// TODO Auto-generated method stub
		if(amt < this.balance + this.OverdraftLimit) {
			this.balance -= amt;
			return true;
		}
		return false;
	}
	
	
	public String toString(){
		return "BALANCE="+this.Balance+", NAME="+this.Name+", OVERDRAFT LIMIT ="+this.OverdraftLimit;
	}

}
}