using System;
namespace FactoryPattern
{
	public class AccountFactory
	{

		public static Account GetAccount(String type, int balance, String name, int overdraftLimit)
		{
			if ("current".Equals(type.ToLower()))
			{
				return new Current(balance, name, overdraftLimit);
			}
			else if ("savings".Equals(type.ToLower()))
			{
				return new Savings(balance, name);
			}

			return null;
		}

	}
}
