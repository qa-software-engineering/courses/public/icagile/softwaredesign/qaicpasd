using System;
namespace FactoryPattern
{
	public class TestFactory {

		public static void Main(String[] args) {
			Account current = AccountFactory.GetAccount("current", 250, "My Current Account", 100);
			Account savings = AccountFactory.GetAccount("SAVINGS", 300, "My Savings Account", 0);
			Console.WriteLine("Factory Current Account Details::" + current);
			Console.WriteLine("Factory Savings Account Details::" + savings);

			// Try to Withdraw within Current account limit
			current.Withdraw(300);
			Console.WriteLine("Factory Current Account Details::" + current);

			// Try to go over Current overdraft limit
			current.Withdraw(300);
			Console.WriteLine("Factory Current Account Details::" + current);

			// Try to Withdraw more than balance
			savings.Withdraw(350);
			Console.WriteLine("Factory Savings Account Details::" + savings);


			// Try to Withdraw less than balance
			savings.Withdraw(200);
			Console.WriteLine("Factory Savings Account Details::" + savings);

		}

	}
}