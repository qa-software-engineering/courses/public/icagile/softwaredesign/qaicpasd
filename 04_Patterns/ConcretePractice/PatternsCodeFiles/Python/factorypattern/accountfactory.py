import current as c, savings as s
class AccountFactory:


	def get_account(acc_type, balance, name, overdraftLimit):

		if ("current" == (acc_type.lower())):
			return c.Current(balance, name, overdraftLimit)
		elif ("savings" == (acc_type.lower())):
			return s.Savings(balance, name)

		return None



