
class Account:

	def __init__(self, balance, name):
		self.__balance = balance
		self.__name = name

	@property
	def balance(self):
		return self.__balance

	@balance.setter
	def balance(self, value):
		self.__balance = value

	@property
	def name(self):
		return self.__name

	@name.setter
	def name(self, value):
		self.__name = value


	def withdraw(self, amt):
		self.__balance -= amt

	def __str__(self) :
		return f"BALANCE={self.balance}, NAME={self.name}"

	def deposit(self, amt):
		self.balance += amt
		return True