"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var account_1 = require("./account");
var Current = /** @class */ (function (_super) {
    __extends(Current, _super);
    function Current(balance, name, overdraft_limit) {
        var _this = _super.call(this, balance, name) || this;
        _this._overdraft_limit = overdraft_limit;
        return _this;
    }
    Object.defineProperty(Current.prototype, "overdraft_limit", {
        get: function () {
            return this._overdraft_limit;
        },
        enumerable: true,
        configurable: true
    });
    Current.prototype.withdraw = function (amt) {
        if (amt < this.balance + this.overdraft_limit) {
            _super.prototype.withdraw.call(this, amt);
            return true;
        }
        return false;
    };
    Current.prototype.toString = function () {
        return "BALANCE=" + this.balance + ", NAME=" + this.name + ", OVERDRAFT LIMIT =" + this.overdraft_limit;
    };
    return Current;
}(account_1.Account));
exports.Current = Current;
