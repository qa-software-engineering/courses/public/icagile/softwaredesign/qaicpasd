"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var accountfactory_1 = require("./accountfactory");
var current = accountfactory_1.AccountFactory.get_account("current", 250, "My Current Account", 100);
var savings = accountfactory_1.AccountFactory.get_account("SAVINGS", 300, "My Savings Account", 0);
console.log("Factory Current Account Details::" + current);
console.log("Factory Savings Account Details::{savings}");
// Try to Withdraw within Current account limit
current.withdraw(300);
console.log("Factory Current Account Details::" + current);
// Try to go over Current overdraft limit
current.withdraw(300);
console.log("Factory Current Account Details::" + current);
// Try to Withdraw more than balance
savings.withdraw(350);
console.log("Factory Savings Account Details::" + savings);
// Try to Withdraw less than balance
savings.withdraw(200);
console.log("Factory Savings Account Details::" + savings);
