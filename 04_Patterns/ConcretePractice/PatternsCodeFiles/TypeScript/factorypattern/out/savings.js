"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var account_1 = require("./account");
var Savings = /** @class */ (function (_super) {
    __extends(Savings, _super);
    function Savings() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Savings.prototype.withdraw = function (amt) {
        if (amt < this.balance) {
            _super.prototype.withdraw.call(this, amt);
            return true;
        }
        return false;
    };
    return Savings;
}(account_1.Account));
exports.Savings = Savings;
