import {Account} from "./account"
import {Current} from "./current"
import {Savings} from "./savings"

export class AccountFactory
{


	static get_account(acc_type:string, balance:number, name:string, overdraftLimit:number):Account
	{
		if ("current" == acc_type.toLowerCase())
		{
			return new Current(balance, name, overdraftLimit);
		}
		else if ("savings" == acc_type.toLowerCase())
		{
			return new Savings(balance, name);
		}
		return null;
	}


}