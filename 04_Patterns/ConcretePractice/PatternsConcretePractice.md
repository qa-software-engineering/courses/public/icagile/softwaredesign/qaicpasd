# Patterns - Concrete Practice

## Design Patterns

Examine the project in the Factory Patterns folder - you will find the same example written in C#, Java, TypeScript and Python.  It is obvious that these are created using the Factory Pattern!

With you partner, discuss alternative design patterns that could have been used to construct the project.

If you have time, try to implement your chosen design pattern, in your chosen language.

*Whole Activity Time Box:* **20 minutes**

## Architecture Patterns

**Activity:** List advantages and disadvantages of different Architecture Patterns

Pair up. Choose a couple of the listed patterns. Discuss. Think about pros and cons of each. Share.

*Whole Activity Time Box:* **10 minutes**
 