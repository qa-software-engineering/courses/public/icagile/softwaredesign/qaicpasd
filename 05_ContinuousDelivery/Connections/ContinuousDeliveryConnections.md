# Continuous Delivery - Connections

## Context

>Feedback is not complete until the system is deployed and in use.
>
>The team’s responsibility for the system doesn’t end when they deliver the product.
>
>Continuous delivery is more than just automating the deployment.

## Questions - Part 1

**Question:** How does Continuous delivery differ from Continuous integration?
*Whole Activity Time Box:* **3 minutes**

**Question:** What are the main benefits of Continuous Delivery?
*Whole Activity Time Box:* **3 minutes**

**Question:** What is the difference between Continuous Delivery and Continuous Deployment?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 2

**Question:** What is the difference between Continuous Delivery and Continuous Deployment?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 3

**Question:** What is DevOps?
*Whole Activity Time Box:* **3 minutes**

## Questions - Part 5

**Question:** What practices does DevOps rely on?
*Whole Activity Time Box:* **3 minutes**

**Question:** What patterns for continuous delivery can you identify?
*Whole Activity Time Box:* **3 minutes**
