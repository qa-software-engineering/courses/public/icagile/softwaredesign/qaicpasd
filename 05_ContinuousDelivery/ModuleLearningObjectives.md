# Continuous Delivery

## Objectives

- Identify the techniques that make up continuous delivery and how they address the "last mile" issues of getting working software deployed into production. These include configuration management and deploy scripts as well as database migration scripts.
- Categorize and explain the problems of a traditional siloed approach to development and operations, as well as the new roles and responsibilities that will emerge from DevOps.  This includes the key principles, methods, practices and cultural considerations that underpin DevOps.
- Introduce common patterns for continuous delivery. This includes the latent code patterns of branch-by-abstraction, feature-toggles, dark-launching, canary-releases and blue-green deployment.

## Outcomes

By the end of this module, a learner can:

- Differentiate among patterns for continuous delivery and implement principles and practices to increase developer-operations collaboration.
- Categorize and explain how continuous delivery contributes to getting products truly DONE.
- Explain and contrast the differences in roles, responsibilities and practices that need to change to increase developer-operations collaboration. 
- Describe the problems common continuous delivery patterns can introduce.
