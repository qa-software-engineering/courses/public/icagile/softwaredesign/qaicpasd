# Continuous Delivery

## Overview

This learning module is to help learners identify the techniques that make up continuous delivery and how they address the "last mile" issues of getting working software deployed into production. These include configuration management and deploy scripts as well as database migration scripts. It will help them categorise and explain the problems of a traditional siloed approach to development and operations, as well as the new roles and responsibilities that will emerge from DevOps.  This includes the key principles, methods, practices and cultural considerations that underpin DevOps. Finally, it introduces common patterns for continuous delivery. This includes the latent code patterns of branch-by-abstraction, feature-toggles, dark-launching, canary-releases and blue-green deployment.

## Concrete Practice

This provides a guided activity to allow learners to demonstrate their skills and understanding of the topic area.

## Code Files

Use the command from the root of the project:
<!-- 
```zsh

git subtree pull --prefix ConcretePractice/BuildToolsCodeFiles https://gitlab.com/qa-software-engineering/learning-modules/ci-cd-cd/build-tools-code-files master --squash

``` -->

This will update any changes to the Code Files in this repo.
