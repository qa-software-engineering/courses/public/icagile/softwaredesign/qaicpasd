# Continuous Delivery - Concepts Part 1

## Objectives

- Identify the techniques that make up continuous delivery and how they address the "last mile" issues of getting working software deployed into production. These include configuration management and deploy scripts as well as database migration scripts.
- Categorize and explain the problems of a traditional siloed approach to development and operations, as well as the new roles and responsibilities that will emerge from DevOps.  This includes the key principles, methods, practices and cultural considerations that underpin DevOps.
- Introduce common patterns for continuous delivery. This includes the latent code patterns of branch-by-abstraction, feature-toggles, dark-launching, canary-releases and blue-green deployment.

## Outcomes

By the end of this module, a learner can:

- Differentiate among patterns for continuous delivery and implement principles and practices to increase developer-operations collaboration.
- Categorize and explain how continuous delivery contributes to getting products truly DONE.
- Explain and contrast the differences in roles, responsibilities and practices that need to change to increase developer-operations collaboration. 
- Describe the problems common continuous delivery patterns can introduce.

---

## Continuous Delivery - Part 1

> Continuous delivery (CD) is a software engineering approach [associated with DevOps,] in which teams produce software in short cycles, ensuring that the software can be reliably released at any time. It aims at building, testing, and releasing software faster and more frequently.
>
>The approach helps reduce the cost, time, and risk of delivering changes by allowing for more incremental updates to applications in production. A straightforward and repeatable deployment process is important for continuous delivery.
> [https://en.wikipedia.org/wiki/Continuous_delivery](https://en.wikipedia.org/wiki/Continuous_delivery)

### Or in Other Words…

>“The ability to get changes - features, configuration changes, bug fixes, experiments - into production or into the hands of users safely and quickly in a sustainable way “
>
>Jez Humble, Author of “Continuous Delivery”, co-author of The DevOps handbook”

Notice the emphasis on ***“safely”*** and ***“sustainable”*** that does not appear explicitly in the *Wikipedia* definition

## Continuous Delivery Can Lead to Higher IT & Business Performance

![Continuous Delivery](../images/CD.png)

**Key concepts:**

- This graphic is from the 2018 Accelerate State of DevOps report and is a great representation of the ability to increase IT and business performance through DevOps.
- It describes the model the researchers tested in 2018’s research.
- It is a structured equation model (SEM), and is a predictive model used to test relationships
- Each box in the figure represents a construct measured in the research, and each arrow represents relationships between the constructs.
- To interpret the model, all arrows can be read using the words predicts, affects, drives, or impacts.
  - For example, IT performance predicts organizational performance. If you see a (-) next to one of the arrows, it means the relationship is negative: Continuous delivery negatively impacts deployment pain. All arrows in the model represent statistically significant relationships.

---

## Continuous Delivery - Part 2

## Continuous Delivery & Continuous Deployment

![Continuous Delivery & Continuous Deployment](../images/CD&CD.png)

**Key concepts:**

- Continuous delivery is sometimes confused with continuous deployment
  - Continuous delivery means that you are able to do frequent deployments but may choose not to do it, usually due to businesses preferring a slower rate of deployment (typically achieved by batching changes into releases)
  - Continuous delivery requires that whenever anyone makes a change that causes an automated test to fail, breaking the deployment pipeline, developers stop the line and bring the system back into a deployable state
- Organizations may even choose to have the continuous integration and deployment system reject any changes (e.g., code or environment commits) that take the code out of a deployable state
- Doing this continually and throughout a development project eliminates the common practice of having a separate integration and test phase at the end of the project, which often get compressed or are skipped entirely, resulting in more technical debt and the downward spiral
- Monitoring is important throughout the deployment pipeline
- In addition to monitoring the production service, we also need to monitor the pre-production environments (e.g. dev, test and staging)

**Why?** So that we detect and correct potential performance problems long before production and so that we can also minimize the cost of correcting those problems

**Resources:**
[https://notafactoryanymore.com/2014/08/19/continuous-everything-in-devops-what-is-the-difference-between-ci-cdcd/]

### Continuous Deployment / Delivery

- Successful, repeatable deployment of code
- After the build has run and been verified then deploy to server
- Automate the server setup / connection
- Code releases can be minute by minute, rather than months at a time

---

## Continuous Delivery - Part 3

## What is DevOps?

![So what is DevOps?](../images/WhatIsDevOps1.png)

**Ask yourself:**

- When does Dev think the work is “Done”?
- When does Op think the work is “Done”?

![So what is DevOps?](../images/WhatIsDevOps2.png)

---

## Three Tiers of Culture Relevant for CD

![3 tiers of culture for DevOps](../images/DevOpsCulture.png)

**Key concepts:**

- Culture is not a monolithic thing.
  - There are different culture level within enterprises that can affect CD.

## Culture Requirements for Effective DevOps

![Culture requirements for effective DevOps](../images/DevOpsCulture2.png)

**Key concepts:**

- There are four requirements for collaborative culture and effective DevOps

## Embrace the 3 ways

![The 3 ways](../images/3Ways.png)

**Key concepts:**

- The **‘Three Ways’** are introduced in **‘The Phoenix Project: A Novel About It, DevOps, And Helping Your Business Win'** by *Gene Kim*, *Kevin Behr* and *George Spafford*
- DevOps practices consistently found in high-performing organizations are emerging and evolving
- The practices that follow align with the principles of The Three Ways

**Additional Resource:**

[http://itrevolution.com/a-personal-reinterpretation-of-the-three-ways/]

## Keep CALMS and do DevOps

![CALMS and DevOps](../images/CALMS.png)

>image from [Keep CALMS and DevOps](https://devblog.axway.com/dev-insights/keep-calms-and-devops/), a blog by David Mckenna (Dec 18, 2018)

---

## Continuous Delivery - Part 4

## DevOps roles

- CDA – Continuous Delivery Architect

**Others include:**

- DevOps evangelist
- Release manager
- Automation architect
- Software developer/tester
- Experience assurance (XA) professional
- Security engineer
- Utility technology player

CDA is a recognised role.

Various organisations have devised their own:

[https://techbeacon.com/devops/7-devops-roles-you-need-succeed]

---

## Continuous Integration - Part 5

## DevOps Practices and Patterns

### DevOps Practices

- Self-service configuration
- Automated provisioning
- Continuous build
- Continuous integration
- Continuous delivery
- Automated release management
- Incremental testing
- Version Control For All Production Artifacts
- Continuous Integration and Deployment
- Automated Acceptance Testing
- Peer Review of Production Changes
- High-Trust Culture
- Proactive Monitoring of the Production Environment
- Win-Win Relationship (and Outcomes) Between Dev and Ops

**References:**

[https://dzone.com/articles/devops-devops-principles]

[https://www.perforce.com/blog/vcs/7-devops-practices-outstanding-results]

### DevOps Patterns

- Feature-toggles (flags)
- Branch-by-abstraction
- Dark-launching
- Canary-releases (testing)
- Blue-green deployment

**References:**

[https://martinfowler.com/bliki/BranchByAbstraction.html]

**Dark launching** is the process of releasing production-ready features to a subset of your users prior to a full release. This enables you to decouple deployment from release, get real user feedback, test for bugs, and assess infrastructure performance.
