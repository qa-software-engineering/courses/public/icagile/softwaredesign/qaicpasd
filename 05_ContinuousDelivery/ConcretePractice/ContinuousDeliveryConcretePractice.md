# Continuous Delivery - Concrete Practice

## Activity - Part 4

**Activity:** Discuss the following:

>How does Continuous Delivery respond to and support each of The Three Ways?
>
>What new roles (and responsibilities) are needed for DevOps?

Discuss in pairs or small groups - a whole group discussion will follow

*Whole Activity Time Box:* **20 minutes**

## Activity - Part 5

**Activity:** Discuss the following:

>What are the pitfalls (if any) of the following DevOps patterns?

Discuss in pairs or small groups - a whole group discussion will follow

*Whole Activity Time Box:* **10 minutes**
