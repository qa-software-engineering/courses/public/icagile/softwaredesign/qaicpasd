# Design-in-the-Large Concepts

## Objectives

- Introduce the roles of coarse-grained design, of dealing with costly-to-change decisions and of evolutionary architecture. This LO includes such things as choosing implementation languages, operating systems and technology frameworks.
- Explain what design is required throughout the different stages of the project. The nature of the work and the level of any deliverable varies depending on whether we are evaluating the initial viability of the project, planning the work, building the product, or preparing for a release.
- Discuss how to sequence work across functional, non-functional and risk aspects. This includes selection strategies such as easiest first, hardest first, highest business value next, highest ROI, technical risk, technical debt, minimized rework and possibly others.

## Outcomes

By the end of this module, a learner can:

- Apply several concepts to determine the level of design needed at various stages of an Agile project and articulate key considerations when sequencing technical work.
- Contrast and explain how much design is too much (identifying "the decisions we're not going to make yet") and also too little (selecting "the decisions we're better off making now").  Recognize and explain that this does not imply or require a "right answer."
- Contrast software design needs at varying stages of an Agile lifecycle.
- Apply two or more methods of sequencing technical work in an Agile software project.

## Making Architecture Matter

[Martin Fowler's take](https://www.youtube.com/watch?v=DngAZyWMGR0)

## Agile Software Development Lifecycle

![Agile Software Development Lifecycle](../images/ASDLC.png)

[http://www.ambysoft.com/essays/agileLifecycle.html](http://www.ambysoft.com/essays/agileLifecycle.html)

## A detailed Agile Software Development Lifecycle

![Detailed Agile Software Development Lifecycle](../images/DASDLC.png)

[http://www.ambysoft.com/essays/agileLifecycle.html](http://www.ambysoft.com/essays/agileLifecycle.html)

## The DAD Agile Lifecycle

**D**isciplined **A**gile **D**elivery

![The DAD Agile Lifecycle](../images/DADALC.png)

[http://www.ambysoft.com/essays/agileLifecycle.html](http://www.ambysoft.com/essays/agileLifecycle.html)

## The DAD Continuous Delivery Lifecycle

![The DAD Continuous Delivery Lifecycle](../images/DADCDLC.png)

[http://www.ambysoft.com/essays/agileLifecycle.html](http://www.ambysoft.com/essays/agileLifecycle.html)

---

## Sequencing the Work

- By value
- Risk and Uncertainty
- Releasability
- Dependencies
- Weighted Smallest Job First – WSJF
- Highest-paid person’s opinion – HiPPO
- Rolling wave planning
- The MoSCoW method

See these websites for more information:

[https://learn.techbeacon.com/units/6-agile-methods-backlog-prioritization](https://learn.techbeacon.com/units/6-agile-methods-backlog-prioritization)

[https://www.romanpichler.com/blog/prioritising-the-product-backlog/](https://www.romanpichler.com/blog/prioritising-the-product-backlog/)

[https://www.mountaingoatsoftware.com/presentations/prioritizing-your-product-backlog](https://www.mountaingoatsoftware.com/presentations/prioritizing-your-product-backlog)

## WSJF

From [SAFe | Provided by Scaled Agile](https://www.scaledagileframework.com/wsjf/):

>Weighted Shortest Job First Weighted Shortest Job First (WSJF) is a prioritization model used to sequence jobs (eg., Features, Capabilities, and Epics) to produce maximum economic benefit. In SAFe, WSJF is estimated as the Cost of Delay (CoD) divided by job size. In a flow-based system, updating priorities continuously provides the best economic outcomes. In such a flow context, it is job sequencing, rather than theoretical, individual job return on investment, that produces the best result. To that end, SAFe applies WSJF to prioritize backlogs by calculating the relative Cost of Delay (CoD) and job size (a proxy for the duration). Backlog priorities are continuously updated based on relative user and business value, time factors, risk reduction and opportunity enablement, and relative job size. WSJF also conveniently and automatically ignores sunk costs, a fundamental principle of Lean economics.
>
>© Scaled Agile, Inc.
<!--Include this copyright notice with the copied content.

Read the FAQs on how to use SAFe content and trademarks here:
https://www.scaledagile.com/about/about-us/permissions-faq/
Explore Training at:
https://www.scaledagile.com/training/calendar/
-->
