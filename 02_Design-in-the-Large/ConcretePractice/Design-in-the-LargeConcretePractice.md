# Design-in-the-Large - Concrete Practice

## Task 1 - Bosun's Chair Prioritisation

- There are some "celebrities" on a ship and it's sinking!
- You are the crew of a rescue ship
- It only has a Bosun's chair which means that you can only rescue 1 person at a time
- You may not be able to rescue all of the passengers before the ship sinks

In what order do you prioritise the rescue?

In essence, we are creating a backlog of celebrities here from...

![Bosun's Chair Group 1](../images/BosunsChair1.png)
![Bosun's Chair Group 2](../images/BosunsChair2.png)
![Bosun's Chair Group 3](../images/BosunsChair3.png)

[Bosun's Chair All - PDF](./BosunsChairGroups.pdf)

*Whole Activity Time Box:* **15 minutes**

## Task 2 - Bosun's Chair Review & Retrospective

What order did you prioritise the rescue and what lead you to those decisions?

Can you align any of the decision making processes to how work is sequenced in a technical project?

*Whole Activity Time Box:* **5 minutes**

## Task 3 - Sequence Technical Work

In your pairs, look at the requirements in the [Cinema Requirements](./CinemaAppRequirements.csv) file.

The customer is keen, in the first instance to have an App that displays some information about the cinema and the films it is showing.  All other requirements unrelated to this goal are secondary at the moment.

Spend **5 minutes** discussing a suitable Sprint backlog for the first iteration of the App.

Spend the next **5 minutes** discussing the level of design that is needed for this stage of the App.

The customer has reserved the option to add/remove requirements from the rest of the backlog, although a booking system is next on their priority list.

Spend the next **5 minutes** discussing the levels of design that should be applied to this and the rest of the backlog.

*Whole Activity Time Box:* **15 minutes**

## Task 4 - Sequencing Retro

Share your thoughts with the other pairs

*Whole Activity Time Box:* **5 minutes**
