# Design-in-the-Large

## Overview

This learning module is to introduce the roles of coarse-grained design, of dealing with costly-to-change decisions and of evolutionary architecture. This LO includes such things as choosing implementation languages, operating systems and technology frameworks. It explains what design is required throughout the different stages of the project. The nature of the work and the level of any deliverable varies depending on whether we are evaluating the initial viability of the project, planning the work, building the product, or preparing for a release. Finally, it discusses  how to sequence work across functional, non-functional and risk aspects. This includes selection strategies such as easiest first, hardest first, highest business value next, highest ROI, technical risk, technical debt, minimized rework and possibly others.
These can be used as part of teaching and/or as the base for video content to be housed on Cloud Academy.  MD can be converted to PPTX using Pandoc.

## Concrete Practice

This provides a guided activity to allow learners to demonstrate their skills and understanding of the topic area.

## Code Files

Use the command from the root of the project:
<!-- 
```zsh

git subtree pull --prefix ConcretePractice/BuildToolsCodeFiles https://gitlab.com/qa-software-engineering/learning-modules/ci-cd-cd/build-tools-code-files master --squash

``` -->

This will update any changes to the Code Files in this repo.
