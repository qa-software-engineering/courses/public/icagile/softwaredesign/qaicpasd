# Design-in-the-Large

## Objectives

- Introduce the roles of coarse-grained design, of dealing with costly-to-change decisions and of evolutionary architecture. This LO includes such things as choosing implementation languages, operating systems and technology frameworks.
- Explain what design is required throughout the different stages of the project. The nature of the work and the level of any deliverable varies depending on whether we are evaluating the initial viability of the project, planning the work, building the product, or preparing for a release.
- Discuss how to sequence work across functional, non-functional and risk aspects. This includes selection strategies such as easiest first, hardest first, highest business value next, highest ROI, technical risk, technical debt, minimized rework and possibly others.

## Outcomes

By the end of this module, a learner can:

- Apply several concepts to determine the level of design needed at various stages of an Agile project and articulate key considerations when sequencing technical work.
- Contrast and explain how much design is too much (identifying "the decisions we're not going to make yet") and also too little (selecting "the decisions we're better off making now").  Recognize and explain that this does not imply or require a "right answer."
- Contrast software design needs at varying stages of an Agile lifecycle.
- Apply two or more methods of sequencing technical work in an Agile software project.
