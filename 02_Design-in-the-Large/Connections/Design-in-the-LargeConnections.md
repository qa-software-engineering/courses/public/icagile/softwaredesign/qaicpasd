# Design-in-the Large - Connections

## Context

>It is a common misconception that in Agile, any form of global design is bad, that all design must be immediate, small and refactored into a macro shape.
>
>Design is a process that continues throughout the life of the project.
>
>Highest-business-value first is a good initial approach to sequencing work, but not always the best.

## Questions

What are the pitfalls of too much upfront design?
*Whole Activity Time Box:* **3 minutes**

## Activities

List architectural decisions, whether they can be made early or late, and the impact of changing your mind later.
*Whole Activity Time Box:* **3 minutes**

Identify typical stages of an Agile lifecycle and design needs for each.
*Whole Activity Time Box:* **3 minutes**
