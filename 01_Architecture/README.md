# Architecture

## Overview

This learning module is to convey the value of enterprise architecture and aligning the IT strategy with the business strategy. Enterprise architecture provides three core services to the organization - strategy, standards and support. It also discusses that different stakeholders have different needs and concerns, resulting in a variety of different viewpoints. Different models may be required for each of these viewpoints, even though they may refer to the same view (or aspect) of the system.
These can be used as part of teaching and/or as the base for video content to be housed on Cloud Academy.  MD can be converted to PPTX using Pandoc.

## Concrete Practice

This provides a guided activity to allow learners to demonstrate their skills and understanding of the topic area.

## Code Files

Use the command from the root of the project:
<!-- 
```zsh

git subtree pull --prefix ConcretePractice/BuildToolsCodeFiles https://gitlab.com/qa-software-engineering/learning-modules/ci-cd-cd/build-tools-code-files master --squash

``` -->

This will update any changes to the Code Files in this repo.
