# Architecture - Concrete Practice

## Activity - The Zachman Framework

A customer has approached you to help them design a website for a new cinema chain.

Use the Zachman Framework (or any other Agile modelling technique of your choice) to help scope their requirements.

This table can be used as a starting point if using Zachman:

| What            | How         | Where                         | Who       | When    | Why                 |
|-----------------|-------------|-------------------------------|-----------|---------|---------------------|
| View show times | Webpage/App | Any Internet-Connected Device | Customers | Anytime | Find out what is on |

*Whole Activity Time Box:* **15 minutes**
