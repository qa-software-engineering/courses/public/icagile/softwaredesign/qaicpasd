# Architecture

## Objectives

- Convey the value of enterprise architecture and aligning the IT strategy with the business strategy. Enterprise architecture provides three core services to the organization - strategy, standards and support
- Discuss that different stakeholders have different needs and concerns, resulting in a variety of different viewpoints. Different models may be required for each of these viewpoints, even though they may refer to the same view (or aspect) of the system.

## Outcomes

By the end of this module, the learner can:

- Articulate the overall impact of system architecture on effective software design in Agile environments.
- Give examples of how enterprise architecture provides strategy, standards and support to organizations.
- Apply one or methods of modelling systems to capture stakeholder needs.
