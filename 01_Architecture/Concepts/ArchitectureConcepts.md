# Architecture

## Objectives

- Convey the value of enterprise architecture and aligning the IT strategy with the business strategy. Enterprise architecture provides three core services to the organization - strategy, standards and support
- Discuss that different stakeholders have different needs and concerns, resulting in a variety of different viewpoints. Different models may be required for each of these viewpoints, even though they may refer to the same view (or aspect) of the system.

## Outcomes

By the end of this module, the learner can:

- **Articulate** the overall impact of system architecture on effective software design in Agile environments.
- **Give** examples of how enterprise architecture provides strategy, standards and support to organizations.
- **Apply** one or more methods of modelling systems to capture stakeholder needs.

## Definitions of Architecture

> The fundamental concepts or properties of a system in its environment embodied in its elements, relationships, and in the principles of its design and evolution

---

## Enterprise Architecture – What is it’s Purpose?

- Complex organizations \(enterprises\) have complex systems
- Enterprise Architecture is about integrating those systems\.\.\. Why?
  - To support the business strategy
- EA provides a strategic context for system evolution\, in response to the constantly changing business environment\, particularly Digital Transformation
- It creates an environment where IT efficiency is balanced against business need

---
![Enterprise Architecture - A Framework](../images/EnterpriseArchitectureFramework.png)

---

## Benefits of Enterprise Architecture

**A more efficient business operation:**

- Lower business operation costs
- More agile organization
- Business capabilities shared across the organization
- Lower change management costs
- More flexible workforce
- Improved business productivity

**A more efficient IT operation:**

- Lower software development, support and maintenance costs
- Increased portability of applications
- Improved interoperability and easier system and network management
- Improved ability to address critical enterprise-wide issues like security
- Easier upgrade and exchange of system components

**Better return on existing investment, reduced risk for future investment:**

- Reduced complexity in the business and IT
- Maximum return on investment in existing business and IT infrastructure
- The flexibility to make, buy, or out-source business and IT solutions
- Reduced risk overall in new investments and their cost of ownership

**Faster, simpler, and cheaper procurement:**

- Buying decisions are simpler, because the information governing procurement is readily available in a coherent plan
- The procurement process is faster - maximizing procurement speed and flexibility without sacrificing architectural coherence
- The ability to procure heterogeneous, multi-vendor open systems
- The ability to secure more economic capabilities

---

## Constraints and The Iron Triangle

### Project Management Forces

![Project Management Forces](../images/iron-triangles.png)

This diagram shows traditional forces that operate in all projects.

**In the left triangle:**

- Common in non-agile – features are fixed, quality is assumed 
  – begin to run out of time or overspend at the expense of quality
- Decrease in quality not intended
  - e.g. unit test code coverage decreases to ensure all features are provided

**In the right-hand triangle:**

- DSDM/Agile Business Consortium say that time and cost are fixed and quality is ensured whilst the number of features depends on what is possible
  - Customer may not get everything that they wanted at the start but what they get will be of good quality
- Requirements/features may change over the course of the allotted time/cost
- Need to prioritise the most important features so they definitely get done
  - Less important may not get done
- Scrum helps with this by providing a Product Owner who manages the backlog (i.e. the order of priority of features)

### The Agile Triangle

This idea was pioneered by Jim Highsmith as an alternative way to approach constraints in Agile projects:

![The Agile Triangle](../images/iron-triangles2.png)

Image taken from the article: [The iron triangle of planning](https://www.atlassian.com/agile/agile-at-scale/agile-iron-triangle) by ***Atlassian***.

This puts a slightly different slant on the constraints of a project by grouping "Resources" instead of simply "Cost"

**Further reading:**

- [Agile Project Management](https://books.google.co.uk/books?hl=en&lr=&id=VuFpkztwPaUC&oi=fnd&pg=PT36&dq=highsmith+agile+project+management&ots=CvBGWkXj7I&sig=DPZgoJ-8cNMK6gzgo3B3PDYsbZc#v=onepage&q=highsmith%20agile%20project%20management&f=false) by Jim Highsmith
- [Beyond Scope, Schedule, and Cost: The Agile Triangle](https://www.linkedin.com/pulse/beyond-scope-schedule-cost-agile-triangle-james-portman/)

---

## Architecture Granularity

**Enterprise Architecture - “computing/architecting in the large”:**

- A pathway leading in the direction defined by the enterprise

**Solution Architecture – “focussed architecture”:**

- Meeting tactical business needs, and benefiting from the governance and tools provided by the enterprise

**Technical Architecture – “specialist technical leadership/specialist application infrastructure knowledge”:**

- Providing technical know-how around networks, infrastructure, operating systems, databases, etc. in a highly distributed world

**Software Architecture – “computing/architecture in the small”:**

- Providing the detailed functionality used in a solution

![Architecture Granularity](../images/ArchitectureGranularity.png)

![Architecture Granularity](../images/ArchitectureGranularity2.png)

---

## Enterprise Architecture 101

More Akin to Town Planning

![More akin to Town Planning](../images/EATownPlanning.png)

## Enterprise Roles

![Enterprise Roles](../images/EnterpriseRoles.png)

Together these Functions have the capability to manage all aspects of an Information System. Who needs architects?

## TOGAF Perspective of Role Relationships

![Capability Planning](../images/TOGAF-RR1.png)

![Business Capability Management](../images/TOGAF-RR2.png)

## TOGAF Architecture Domains

![Architecture Domains](../images/TOGAF-AD.png)

## Abstract View - Enterprise Architecture

![Abstract](../images/EA-Abstract.png)

### Enterprise Architecture

A strategic approach to architecture that addresses a whole enterprise. The highest level, widest scope, longest term kind of architecture.

1: documentation describing the structure and behaviour of an enterprise and its information systems.
Or 2: a process for describing an enterprise and its information systems and planning changes to improve the integrity and flexibility of the enterprise.

**Operating Domain/Operating Model** – see Ross, Weil and Robertson in their book “Enterprise Architecture as Strategy”
**Value Chain/Value Chain Model** – see Michael Porter in his book “Competitive Advantage: Creating and Sustaining Superior Performance”
**Business Domain/Business Model** – see Alexander Osterwalder’s “Business Model Canvas”

---

## Building Blocks

**A building block:**

- is a package of functionality defined to meet the business needs across an organisation
- has a defined boundary and is generally recognisable as "a thing" by domain experts
- may interoperate with other, inter-dependent building blocks

A *good* building block has the following characteristics:

- It considers implementation and usage, and evolves to exploit technology and standards
- It may be assembled from other building blocks
- It may be a sub-assembly of other building blocks
- Ideally a building block is re-usable and replaceable, and well specified

The way in which assets and capabilities are assembled into building blocks will vary widely between individual architectures. Every organization must decide for itself what arrangement of building blocks works best for it.

## Architecture Building Blocks (ABBs)

**Characteristics:**

- Capture architecture requirements; e.g. business, data, application, and technology requirements
- Direct and guide the development of Solution Building Blocks (SBBs)

**Specification Content:**

- Fundamental functionality and attributes: semantic, unambiguous, including security capability and manageability
- Interfaces: chosen set, supplied
- Interoperability and relationship with other building blocks
- Dependent building blocks with required functionality and named user interfaces
- Map to business/organizational entities and policies

## Solution Building Blocks (SBBs)

**Characteristics:**

- Define what products and components will implement the functionality
- Define the implementation
- Fulfil business requirements
- Are product or vendor-aware

**Specification Content:**

- Specific functionality and attributes
- Interfaces
- Required SBBs used with required functionality and names of the interfaces used
- Mapping from the SBBs to the IT topology and operational policies
- Specifications of attributes shared across the environment (not to be confused with functionality) such as security, manageability, localisability, scalability
- Performance, configurability
- Design drivers and constraints, including the physical architecture
- Relationships between SBBs and ABBs

---

## Current and Target State

![Current and Target State](../images/Current&TargetState.png)

## Building Blocks and the Enterprise Continuum

![Solutions Continuum](../images/SolutionsContinuum.png)

## Business Capabilities and Supporting Technologies – Detail

Badly defined requirements have a downward pressure

- Effects Cost, Time and, importantly, the scope of impact

![Requirements](../images/Requirements.png)

The triangle shows inverse relationships

- A single piece of string, looped back on itself
- The string must have four points
  - The centre and one for each corner of the triangle
- Pull towards one of the corners, it shortens those parts of the string that are directed towards the other corners
  - E.g. the closer you are to the requirements point, the narrower the width of the triangle (shown with the anchor), so cost and time anchors increase
- The more well defined the requirements (move away from the requirements point), the smaller the costs and time to deliver, why?
  - It has been shown that the understanding of the impact on current systems is better understood if the requirements are well understood and defined

---

# Solution Architecture 101

Akin to Building Architecture

![Akin to Building Architecture](../images/SolutionArchitecture.png)

## ABBs to SBBs

![ABBs to SBBs](../images/ABBtoSBBs.png)

## What they do

Act as a bridge from the Enterprise World to the Solution World

![Enterprise to Solution bridge](../images/EnterpriseToSolutionBridge.png)

Can be a specialist in delivering solutions using certain technologies

![Solution Specialists](../images/SolutionSpecialists.png)

## Solution Architect Responsibilities

Should develop and maintain good client relationships
Should understand how to cost solutions
Must have good soft skills

- Stakeholder management techniques
- Interviewing and reporting skills

Should be able to write bid responses as well as bids

---

# Technical Architecture 101

Akin to Infrastructure Architecture

![Akin to Infrastructure Architecture](../images/TechnicalArchitecture.png)

## ABBs to SBBs

![ABBs to SBBs](../images/TA-ABB-SBB.png)

## What they do

Act as the bridge from the Enterprise World to the Solution World

![Enterprise World to Solution World bridge](../images/EW-SW.png)

Should be specialist certain technologies, allowing them to advise

![Technical Architecture Providers](../images/TA-Providers.png)

## Technical Architecture Responsibilities

- Understand the distributed nature of the world in which systems exist
- Understand the quality attributes of each of the available solutions

![Techincal Architecture Responsibilities](../images/TA-Responibilities.png)

- Should develop and maintain good client relationships
- Should develop and maintain good business relationships
- Should understand how to cost solutions
- Must have good soft skills
  - Stakeholder management techniques
  - Interviewing and reporting skills
- Should be able to write bid responses as well as bids

---

# Architectures in General

>Those who don't know history are doomed to repeat it.”
>Edmund Burke

Don’t ignore something because it’s considered to be old school

![Architecture History](../images/Arch-Hist.png)

## Technology Pendulum

Architecture styles come and go and come back again.
The biggest driver for this pendulum swing is bandwidth

- Give users more bandwidth then push more down it
  - Consider the internet from dial-up to broadband
    - Simple html
    - Images
    - Sound
    - Video
    - Games

James Gosling of Sun Microsystems noted that as they (Sun) gave users more bandwidth, they (users) found more creative ways of consuming that bandwidth

![Technology Pendulum](../images/TechPendulum.png)

## Before the 2-tier Architecture

- Prevalent in late 1980s and early 1990s
  - Individual computers with no network connectivity
  - Couldn’t share resources
  - Physically had to walk from machine to machine with disks in order to share resources

![Pre 2-tier Architecture](../images/Pre2tier.png)

## 2-tier Architecture

- Prevalent in late 1980s and early 1990s
- Common architecture style for small companies and start-ups
- Birthed out of Novell Netware technology

Problem with this model is:

- Client side security
- Client side transactions
- Client side connections
- Client side business logic
- Not scalable
- Not resilient
- Not secure
- Difficult to maintain

![2-tier Architecture](../images/2-tier.png)

What happens if a desktop wants to connect to multi DB Servers?

## 3-tier Architecture

The 2-tier architecture comes with some serious issues as described on the previous slide
By introducing a mid-tier component we can:

- Improve the security by moving it to the application server
  - mid-tier authenticates users via an authentication server, then uses anonymous logins to the DB, reduced logins and interaction with the DB
- Move business logic and transactional behaviour to the mid-tier
- There may be multiple DB Hosts, Application Servers can be put into farms, reference data of DBs can be cached reducing traffic to DB

![3-tier Architecture](../images/3-tier.png)

## N+-tier Architecture

With the large investment in the mid-tier, you will want to reuse as much of this core logic if new channels to clients are introduced
Have as little business logic in the web server as possible, the web server is simply an adapter to the browser

The mode is:

- More scalable
- More resilient
- More performant
- More secure

![N-tier Architecture](../images/n-tier.png)

## Distributed Systems

When we partition our world as described in previously, these are typically called **Distributed Systems**, more accurately defined as:

>Two or more nodes connected together with some kind of communications infrastructure, each node housing processing capability, each node executing processes that are communicating with other processes that reside on other nodes

What is a node?

- A piece of hardware with processing capability

![Distributed Systems](../images/DistributedSystems.png)

---

## Resilience

- The capacity to recover quickly from difficulties
- Toughness, staying power
- IT Infrastructure that:
  - Embraces change
  - Mitigates any disaster
  - Mitigates any interruption
  - Creates an uninterrupted environment for the business

## Disaster Recovery

The practice of ensuring there is a Disaster Recovery Plan (DRP) detailing Recovery Point Objective (RPO) and Recovery Time Objective (RTO), and a Disaster Recovery Test (DRT)

A key part of security planning, aimed at protecting an organisation from the effects of significant negative events:

- Application or virtual machine failure
- Communications failure
- Chassis failure, causing a single or multiple hosts to fail
- Rack failure
- Data centre and/or building disaster
- Citywide, regional, national disasters

As businesses have become more reliant on high availability, their tolerance for downtime has decreased

## Disaster Recovery Plan

A detailed manifest specifying how to restore systems back to normal
Should be clear and simple, covering:

- Who the main recovery personnel are
- Emergency response actions
- Diagrams of infrastructure and sites
- Software required to perform the recovery

Additionally, it should detail:

- How to deal with press
- Insurance coverage
- How to deal with financial and legal issues
- Import external contacts

## Disaster - Recovery Point Objective (RPO)

- The maximum age of files that an organisation must recover from backup storage to resume normal operations
- Determines the minimum frequency of backups
- So an RPO of four hours means backups every four hours

![RPO](../images/RPO.png)

## Disaster - Recovery Time Objective (RTO)

- The maximum downtime an organisation can handle
- Following a disaster, the maximum time has to recover files from backup storage and restore normal operations

To meet tight RTO organisations you need to consider:

- The placement of data centres
- Making use of the recovery in place methods to minimise the use of networks to recover data
- Making use of replication to speed up recovery times

![RTO](../images/RTO.png)

---

## High Availability Services

One of the many Non-Functional Requirements (also referred to as QoS – Quality of Service attributes)

**Availability:**

It is that period of time when a system is operating at the desired level of functionality
Availability is measured as a factor of reliability

- The higher the availability, the higher the reliability

It is important to agree on:

- Uptime
  - A system can be present but not functionally active – the network is there but there is no internet connection
- Availability
  - The system is present and functionally active – the network is there and working and there is an internet connection

## Availability – Scheduled and Unscheduled Downtime

- Scheduled downtime
- Unscheduled downtime
- Excluding scheduled downtime from calculations
  - Can be misleading – not many organisations achieve continuous availability, it’s very expensive
  - Can be achieved by running systems in degraded modes
  - Will make use of dual redundant temporary systems whilst scheduled work is being done

## Achieving High Availability

This requires:

- The elimination of single points of failure
- Detection of failures as they occur – active monitoring
- Reliable Crossover Points

**NOTE:**
There are many different configurations such as clustering, failover clustering, load balancing, to name a few, that can be applied to achieving high availability.  We only consider a few concepts here as a means of stressing the importance of design for high availability

## High Availability – Remove Single Points of Failure

![Single Points of Failure](../images/SPOF.png)

In this example having only one database could constitute a single point of failure:

- The loss of the DB would render the systems inoperable
- The single DB may not be able to cope the performance demands
- Running transactional and reporting systems of the single DB would create a performance bottleneck

## High Availability – Detection of Failures as they occur

![Detection of Failures](../images/DOF.png)

Monitoring has to be factored into the overall architecture of a system and cannot be done as an after thought.

**In the above example:**

- Application, web and databases servers are monitored
- Network paths and performance are monitored
- Additionally monitored components
- Power systems, possibly client systems, the software running on servers
- Cost is directly related to volume of monitoring

![Cost-Importance Graph](../images/CostImportance.png)

## High Availability – Reliable Crossover Points

![Reliable Crossover Points](../images/RCP.png)

To achieve high levels of availability it is usual for systems to be replicated across multiple zones/regions

- Building in redundancy

Those points where events/messages are rerouted to a replicated system in another zone/region are referred to as Crossover Points

- They must be reliable otherwise they become single points of failure themselves

## Active/Passive and Active/Active Models

**Active/Active:**

An application is run on all servers processing. In the event of one server becoming unavailable, the surviving processes continue processing and assumes the role as the primary processing unit.

**Active/Passive (hot standby):**

An application is deployed on all servers but only one server is processing the data. In the event of the active server becoming unavailable, traffic is switched to one of the standby servers and the now active server assumes the role as the primary processing unit.

---

## Modelling the Views and Viewpoints of Stakeholders

![Agile Requirements Modelling](../images/ARM.png)

Excerpts from: [Agile Requirements Modelling](http://agilemodeling.com/essays/agileRequirements.htm)

For this learning module, we are only really concerned with the top green box of this diagram.



---

## Simple User Stories

![Simple User Stories](../images/SimpleUserStories.png)

---

## Simple Point-form Use Case

![Simple Point-Form Use Case](../images/SimplePointFormUseCase.png)

---

## Modelling Techniques - Zachman

>The Zachman Framework is an enterprise ontology and is a fundamental structure for Enterprise Architecture which provides a formal and structured way of viewing and defining an enterprise. The ontology is a two dimensional classification schema that reflects the intersection between two historical classifications. The first are primitive interrogatives: What, How, When, Who, Where, and Why. The second is derived from the philosophical concept of reification, the transformation of an abstract idea into an instantiation. The Zachman Framework reification transformations are: Identification, Definition, Representation, Specification, Configuration and Instantiation.
>
> John Zachman - A concise definition of the Zachman Framework (2008) - via Wikipedia

![The Zachman Framework](../images/ZF.jpg)

---

## An Ever Changing World

- The pace of change is increasing every couple of years
  - We used to able to measure by the decades
  - Norms are being thrown out
- With the internet, every idea has an audience
  - Ideas that you and I might consider to be ridiculous or we might be asking ourselves “who would ever want that”, find an audience
  - Who would have thought that the products below would have found an audience (sample of disruptors)
    - Tinyurl
    - Twitter
    - Pinboard and Delicious
- All of these changes are underpinned by one common thing
  - Technology – changing our world and the way in which we interact with our world
- Ignoring it won’t stop it

---

## Technology alone won’t make you succeed

- There have been lots of great technological ideas over the past 30 years that have gone to the wall
  - Creative Zen – preferred media player of most musicians, but Creative failed to understand their market and develop an ecosystem as Apple has
  - MySpace has not been able to reinvent itself since 2008, now overtaken by Instagram
- There have been and still exist great technological ideas
  - Search engines – Yahoo!, Bing, and Google (Lycos, ask, Netscape and AOL)
  - Amazon – books, other products and now groceries
  - Monzo
- So why did Apple get it so right and Creative get it so wrong?

---

## Growth in the Ecosystem Perspective

Steve Jobs is credited with bringing the concept of the ecosystem to the forefront businesses minds
- [http://obamapacman.com/2011/09/time-magazine-apple-ecosystem-infographic/](http://obamapacman.com/2011/09/time-magazine-apple-ecosystem-infographic/)
- Google search: Steve Jobs Apple’s ecosystem

![Apple's Ecosystem](../images/Ecosystem.png)

Steve Jobs understood the power of:

- The Brand
- Marketing
- The ecosystem – how to keep customers coming back
- Producing the best product
- Ensuring that everything should be connected and could be using technology

---

## Four domains working as one

Attempts to address the complex issues of the ecosystem can be viewed through the prism of the four domains and their symbiotic relationship to one another.

![Four Domains As One](../images/FourDomainsAsOne.png)

---
