# Architecture

## Context

>Software development is only useful if it helps the organization achieve its goals.
>
>There is no one-size-fits-all perspective that will meet the needs of all stakeholders.

## Questions

How would you define Architecture?
*Whole Activity Time Box:* **3 minutes**

What impact does architecture have on a development team?
*Whole Activity Time Box:* **3 minutes**

What does architecture provide for development teams?
*Whole Activity Time Box:* **3 minutes**

How can a customer's requirements be captured?
*Whole Activity Time Box:* **3 minutes**
