# Technical Debt - Connections

## Context

>The cruft that accrues in the code either from neglect or intentionally when hitting deadlines reduces code malleability – it is a technical debt that needs addressing.
>
>Programmers all too often complain that they have to create technical debt just because the boss says to, even despite the boss saying not to.

## Question Part 1

How would you define Technical Debt?
*Whole Activity Time Box:* **3 minutes**

## Question Part 2

What are the symptoms of Technical Debt?
*Whole Activity Time Box:* **3 minutes**

## Activities Part 2

What are the Pros and Cons of addressing Technical Debt?
*Whole Activity Time Box:* **5 minutes**

Identify different approaches to reducing Technical Debt. For each, classify as reactive or proactive.
*Whole Activity Time Box:* **5 minutes**
