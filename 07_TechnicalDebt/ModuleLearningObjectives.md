# Technical Debt

## Objectives

- Describe the concept of technical debt and its mounting negative impact on productivity, and discuss how to recognize this technical debt. This includes understanding technical debt born from taking short cuts, learning better ways of doing things and gaining new technical capabilities that help simplify existing code.  This also includes recognizing technical debt by symptoms such as high cost in adding new functionality, high frequency of new bugs when changing code, difficulty in automating testing and code that is difficult to understand.
- Model dialogue with various stakeholders about technical debt and what (if anything) should be done about it.

## Outcomes

By the end of this module, a learner can:

- Analyze the impact of technical debt throughout an Agile lifecycle and enumerate pro / con approaches to addressing it with stakeholders.
- Identify and classify the symptoms of technical debt in a codebase, explain why it happens, why it is problematic and how to prevent or reduce it.
- Differentiate approaches to technical debt reduction based on costs / benefits, technical environments and project/product constraints.
