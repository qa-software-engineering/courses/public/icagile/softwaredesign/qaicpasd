# Technical Debt - Concrete Practice

Go back to the code base and list of issues you found when working on [**Simple Design**](../../03_SimpleDesign/ConcretePractice/SimpleDesignConcretePractice.md).

The smells identified here all result in technical debt.

For each of the issues you identified, explain how you would reduce the technical debt caused.

You will be asked to feedback to the whole group.

*Whole Activity Time Box:* **10 minutes**
