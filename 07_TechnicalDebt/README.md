# Technical Debt

## Overview

This learning module is to encourage learners to practice and apply strategies and techniques for automating acceptance tests without using the user interface, including the architectural support required.  They should be enabled to explain strategies and techniques for building maintainable and intention-revealing automated acceptance tests when it is not possible or practical to bypass the user interface. This includes using adapters or wrappers around the user interface so that good UI-agnostic acceptance tests can be used.  Finally, they will be able to describe the role of test automation in verifying cross-functional attributes, including capacity and response time, security, etc., and how to fit these into an Agile process.

These can be used as part of teaching and/or as the base for video content to be housed on Cloud Academy.  MD can be converted to PPTX using Pandoc.

## Concrete Practice

This provides a guided activity to allow learners to demonstrate their skills and understanding of the topic area.

## Code Files

Use the command from the root of the project:
<!-- 
```zsh

git subtree pull --prefix ConcretePractice/BuildToolsCodeFiles https://gitlab.com/qa-software-engineering/learning-modules/ci-cd-cd/build-tools-code-files master --squash

``` -->

This will update any changes to the Code Files in this repo.
