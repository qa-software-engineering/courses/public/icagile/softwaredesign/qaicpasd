# Technical Debt - Concepts

## Objectives

- Describe the concept of technical debt and its mounting negative impact on productivity, and discuss how to recognize this technical debt. This includes understanding technical debt born from taking short cuts, learning better ways of doing things and gaining new technical capabilities that help simplify existing code.  This also includes recognizing technical debt by symptoms such as high cost in adding new functionality, high frequency of new bugs when changing code, difficulty in automating testing and code that is difficult to understand.
- Model dialogue with various stakeholders about technical debt and what (if anything) should be done about it.

## Outcomes

By the end of this module, a learner can:

- Analyze the impact of technical debt throughout an Agile lifecycle and enumerate pro / con approaches to addressing it with stakeholders.
- Identify and classify the symptoms of technical debt in a codebase, explain why it happens, why it is problematic and how to prevent or reduce it.
- Differentiate approaches to technical debt reduction based on costs / benefits, technical environments and project/product constraints.

## Technical Debt

>“The implied cost of additional rework caused by choosing an easy solution now instead of using a better approach that would take longer.”

[Wikipedia](https://en.wikipedia.org/wiki/Technical_debt)

>Technical Debt: the typically unpredictable overhead of maintaining the product, often caused by less than ideal design decisions, contributing to the total cost of ownership. May exist unintentionally in the Increment or introduced purposefully to realize value earlier.

[The Scrum Glossary on scrum.org](https://www.scrum.org/resources/scrum-glossary#:~:text=Technical%20Debt%3A%20the%20typically%20unpredictable,purposefully%20to%20realize%20value%20earlier.)

---

## Technical Debt?

**Fix...**

![Fix](../images/Fix.png)

**...or Fester**

![Fester](../images/Fester.png)

---

## The Technical Debt Quadrant

![Technical Debt Quadrant](../images/TDQ.png)

Read [more](https://martinfowler.com/bliki/TechnicalDebtQuadrant.html) on this from Martin Fowler.
